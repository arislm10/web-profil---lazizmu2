<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {


	public function index()
	{
		$data['content'] = 'profile/v_latarBelakang';
		$data['latar_belakang'] = $this->db->get_where('tb_profile',array('id_profile'=>2))->row();
		$this->load->view('layout/temp',$data);
	}
	public function update_latarBelakang(){

			$key = $this->input->post('keyword');
			$judul = $this->input->post('title');
			$isi = $this->input->post('content');
			$date = $this->input->post('date');
			$data = array(
				
				'keyword' => $key,
				'title' => $judul,
				'content' => $isi,
				'tanggal_update'   => $date
			);
			if(isset($data)){
				$this->db->where('id_profile', 2);
				$u = $this->db->update('tb_profile', $data);
				if($u == 1) {
					$this->session->set_flashdata('success', 'Data Berhasil diubah'); 
					redirect('profile');
				}
			}else {
				$this->session->set_flashdata('error', 'Data gagal Masuk'); 
				redirect('profile');
		}
		
	}
	public function visi_misi()
	{
		$data['content'] = 'profile/v_visiMisi';
		$data['visimisi'] = $this->db->get('tb_profile')->row();
		$this->load->view('layout/temp',$data);
	}

	public function update_visiMisi(){

		$key = $this->input->post('keyword');
		$judul = $this->input->post('title');
		$isi = $this->input->post('content');
		$date = $this->input->post('date');
		$data = array(
			
			'keyword' => $key,
			'title' => $judul,
			'content' => $isi,
			'tanggal_update'   => $date
		);
		if(isset($data)){
			$this->db->where('id_profile', 1);
			$u = $this->db->update('tb_profile', $data);
			if($u == 1) {
				$this->session->set_flashdata('success', 'Data Berhasil diubah'); 
				redirect('profile/visi_misi');
			}
		}else {
			$this->session->set_flashdata('error', 'Data gagal Masuk'); 
			redirect('profile/visi_misi');
	}
	
}
	
	public function struktur_organisasi()
	{
		$data['content'] = 'profile/v_strukturOrganisasi';
		$data['struktur'] = $this->db->get_where('tb_profile',array('id_profile'=>3))->row();
		$this->load->view('layout/temp',$data);
	}
	public function update_struktur(){

		$key = $this->input->post('keyword');
		$judul = $this->input->post('title');
		$isi = $this->input->post('content');
		$date = $this->input->post('date');
		$data = array(
			
			'keyword' => $key,
			'title' => $judul,
			'content' => $isi,
			'tanggal_update'   => $date
		);
		if(isset($data)){
			$this->db->where('id_profile', 3);
			$u = $this->db->update('tb_profile', $data);
			if($u == 1) {
				$this->session->set_flashdata('success', 'Data Berhasil diubah'); 
				redirect('profile/struktur_organisasi');
			}
		}else {
			$this->session->set_flashdata('error', 'Data gagal Masuk'); 
			redirect('profile/sturktur_organisasi');
	}
	
}
	public function kebijakan_strategis()
	{
		$data['content'] = 'profile/v_kebijakanStrategis';
		$data['ks'] = $this->db->get_where('tb_profile',array('id_profile'=>4))->row();
		$this->load->view('layout/temp',$data);

	}
	public function update_kebijakanStrategis(){

		$key = $this->input->post('keyword');
		$judul = $this->input->post('title');
		$isi = $this->input->post('content');
		$date = $this->input->post('date');
		$data = array(
			
			'keyword' => $key,
			'title' => $judul,
			'content' => $isi,
			'tanggal_update'   => $date
		);
		if(isset($data)){
			$this->db->where('id_profile', 4);
			$u = $this->db->update('tb_profile', $data);
			if($u == 1) {
				$this->session->set_flashdata('success', 'Data Berhasil diubah'); 
				redirect('profile/kebijakan_strategis');
			}
		}else {
			$this->session->set_flashdata('error', 'Data gagal Masuk'); 
			redirect('profile/kebijakan_strategis');
		}
	
	}
}
