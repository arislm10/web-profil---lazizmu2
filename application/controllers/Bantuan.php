		<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bantuan extends CI_Controller {

	public function index()
	{
        $data['content'] = 'bantuan/add_bantuan';
		$this->load->view('layout/clientTmp',$data);
		}
public function view_bantuan()
	{
        $data['content'] = 'bantuan/v_bantuan';
        $this->db->order_by("id_bantuan", "desc");
        $data['bantuan'] = $this->db->get('tb_bantuan')->result();
		$this->load->view('layout/temp',$data);
  }
  public function cetak_bantuan()
	{
         $this->db->order_by("id_bantuan", "desc");
        $data['bantuan'] = $this->db->get('tb_bantuan')->result();
		$this->load->view('bantuan/cetak_bantuan',$data);
  }
  public function hapus_bantuan($id)
	{
       $this->db->delete('tb_bantuan', array('id_bantuan' => $id));
       $this->session->set_flashdata('error', 'Data Terhapus!'); 
        redirect('bantuan/view_bantuan');
  }
public function tambah_bantuan()
	{
		$nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $pekerjaan = $this->input->post('pekerjaan');
        $penghasilan = $this->input->post('penghasilan');
        $pnd = $this->input->post('pengajuan');
   	    $don=implode(",",$pnd);
        $config['upload_path']          = './gambar_artikel/';
        $config['allowed_types']        = 'svg|gif|jpg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 3000; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('kk')) {
           $kk= $this->upload->data("file_name");
        }
            if ($this->upload->do_upload('ktp')) {
           $ktp= $this->upload->data("file_name");
        }

        $data = array(

        	'nama_lengkap' => $nama,
        	'alamat'=> $alamat,
        	'telp'=> $telp,
        	'pekerjaan' => $pekerjaan,
        	'penghasilan' => $penghasilan,
        	'jenis_pengajuan' => $don,
        	'kk'=> $kk,
        	'ktp' => $ktp
        );
        print_r($data);

       $this->db->insert('tb_bantuan',$data);
       $this->session->set_flashdata('success', 'Terimakasih Data Telah Kami Simpan'); 
       redirect('bantuan');
    }
	}