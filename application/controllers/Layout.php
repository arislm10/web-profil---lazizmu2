<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends CI_Controller {

    function __construct(){

        parent::__construct();
        cek_login();
        cek_hakakses(array(1));
    }

    public function index()
    {
        $data['content'] = 'layout';
        $data['contact'] = $this->db->get('tb_contact')->row();
        $data['laykeg'] = $this->db->get('tb_layout_kegiatan')->result();
        $data['head_image'] = $this->db->get_where('tb_layout_gambar',array('id_letak' => 1))->row();
        $data['background_image'] = $this->db->get_where('tb_layout_gambar',array('id_letak' => 2))->row();
        $data['event_image'] = $this->db->get_where('tb_layout_gambar',array('id_letak' => 3))->row();
        $this->load->view('layout/temp',$data);
    }

    public function ubahKontak()
    {   
        $data['content'] = 'kontak/ubah_kontak';
        $data['kontak'] = $this->db->get('tb_contact')->row();
        $this->load->view('layout/temp',$data);
    }
    public function ubahKontakProses(){
        
        $kontak = $this->input->post('kontak');
        $fb = $this->input->post('fb');
        $tw = $this->input->post('tw');
        $ig = $this->input->post('ig');
        $wa = $this->input->post('wa');
        $telp = $this->input->post('telp');
        $desc = $this->input->post('desc');
        $id = $this->input->post('id');
        $data = array(
            'nama_contact' => $kontak,
            'fb' => $fb,
            'twitter' => $tw,
            'whatsapp' => $wa,
            'ig' => $ig,
            'no_telp' => $telp,
            'deskripsi' => $desc
        );

        if(isset($data)){
            $this->db->update('tb_contact', $data, "id_contact = $id ");
            $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
            redirect('layout');
        }else {
            $this->session->set_flashdata('error', 'Data gagal diubah'); 
            redirect('layout');
        } 

    }

    public function edit_lihat_kegiatan($id){

        $data['content'] = 'ubah_kegiatan';
        $data['keg'] = $this->db->get_where('tb_layout_kegiatan',array('id_layout'=>$id))->row();
        $this->load->view('layout/temp',$data);

    }

    
    public function hapus_lihat_kegiatan($id){

        $this->db->delete('tb_layout_kegiatan', array('id_layout' => $id));
        $this->session->set_flashdata('success', 'Berhasil hapus data!'); 
        redirect('layout');

    }

    public function ubah_kegiatan(){
        $kegiatan = $this->input->post('kegiatan');
        $desc = $this->input->post('deskripsi');
        $id = $this->input->post('id');
        $data = array(
            'nama_kegiatan' => $kegiatan,
            'deskripsi' => $desc
        );

        if(isset($data)){
            $this->db->update('tb_layout_kegiatan', $data, "id_layout = $id ");
            $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
            redirect('layout');
        }else {
            $this->session->set_flashdata('error', 'Data gagal diubah'); 
            redirect('layout');
        } 
    }
    public function tambah_kegiatan(){

        $kegiatan = $this->input->post('kegiatan');
        $desc = $this->input->post('deskripsi');

        $data = array(
            'nama_kegiatan' => $kegiatan,
            'deskripsi' => $desc
        );

        if(isset($data)){
            $this->db->insert('tb_layout_kegiatan',$data);
            $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
            redirect('layout');
        }else {
            $this->session->set_flashdata('error', 'Data gagal diubah'); 
            redirect('layout');
        }

    }

    public function ubah_gambarBackground(){
        $letak = $this->input->post('title');
        $id = $this->input->post('id');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'svg|gif|jpg|png';
        $config['overwrite']            = true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'nama_letak' => 'backgroud_awal',
            'gambar' => $image
        );

        if(isset($data)){
           $this->db->where('id_letak', $id);
           $update = $this->db->update('tb_layout_gambar', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('layout');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('layout');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('layout');
        }
    }
    public function ubah_gambarHead(){
        $letak = $this->input->post('title');
        $id = $this->input->post('id');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|svg|png';
        $config['overwrite']            = true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'nama_letak' => 'header',
            'gambar' => $image
        );

        if(isset($data)){
           $this->db->where('id_letak', $id);
           $update = $this->db->update('tb_layout_gambar', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('layout');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('layout');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('layout');
        }
    }
    public function ubah_gambarEvent(){
        $letak = $this->input->post('title');
        $id = $this->input->post('id');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|svg|png';
        $config['overwrite']            = true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'nama_letak' => 'backgroud_awal',
            'gambar' => $image
        );

        if(isset($data)){
           $this->db->where('id_letak', $id);
           $update = $this->db->update('tb_layout_gambar', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('layout');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('layout');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('layout');
        }
    }
}