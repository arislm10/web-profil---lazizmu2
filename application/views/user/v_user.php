
<section class="content">
      <div class="row">
        <div class="col-md-12">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
+Tambah User
</button>
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('user/tambah_user') ?>" enctype="multipart/form-data" method="post">
      <div class="modal-body">
      <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" id="inputEmail3" required  placeholder="email">
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                  <input type="password" name="password" class="form-control" id="inputEmail3" required placeholder="password">
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                <label for="" class="col-sm-2 control-label">Level</label>
                <div class="col-sm-10">
                  <select name="level" class="form-control">
                  <option value="1">administrator</option>
                   <option value="2">Operator</option>
                  </select>
                </div>
                </div>
              
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
                    </div>
                </div>
                </div>
 
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>email</th>
                  <th>level</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($user->result() as $g){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $g->email  ?></td>
                  <td><?= $g->nama_level ?></td>
                  
                  <td><a href="<?= site_url('user/edit_lihat/').$g->id_admin ?>" class="btn btn-warning">Edit</a>|| <a href="<?= site_url('user/hapus_user/').$g->id_admin ?>" class="btn btn-danger">Hapus</a> </td>
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>