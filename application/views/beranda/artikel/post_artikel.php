
<section class="content">
      <div class="row">
        <div class="col-md-12">
        <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
        <form action="<?= site_url('artikel/post_artikel_proses') ?>" enctype="multipart/form-data" method="post">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">POST ARTIKEL</h3>
             
      <div class="form-group">
      <label for="usr">Judul:</label>
      <input type="text" class="form-control" id="usr" name="title" required="">
      </div>
      <div class="form-group">
                  <label>Kategori</label>
                  <select name="kategori" class="form-control">
                  <?php 
                    $ka = $this->db->get('tb_kategori');
                    foreach($ka->result() as $val){
                  ?>
                    <option value="<?= $val->id_kategori ?>"><?=  $val->nama_kategori?></option>
                    <?php } ?>

                  </select>
                </div>
        <input type="hidden" name="email" value="<?= $this->session->userdata('email'); ?>">
      <div class="form-group">
      <label for="usr">Label:</label>
      <select id="select" name="label[]" class="form-control"  multiple="multiple" required="">
      <option value="infaq">infaq</option>
      <option value="lazizmu">lazizmu</option>
      <option value="muhammadiyah">muhammadiyah</option>
      <option value="sedekah">sedekah</option>
      <option value="zakat">zakat</option>
      <option value="rakerwil">rakerwil</option>
      <option value="kisah_inpirasi">kisah_inspirasi</option>
      </select>
      </div>
      <div class="form-group">
      <label for="usr">Gambar:</label>
      <input type="file" class="form-control" id="usr" name="gambar" >
      </div>
      <div class="form-group">
      <label for="usr">Keyword:</label>
      <input type="text" class="form-control" id="usr" name="keyword" required="">
      </div>
      <input type="hidden" name="date" value="<?=date("Y-m-d")  ?>" >
      </div>
      <label for="usr">Isi Konten:</label>
      <div class="box-body pad">
                    <textarea id="editor1" name="content" rows="10" cols="80" style="visibility: hidden; display: none;" required="">
                    </textarea>
                    <br>
                    <button type="submit" class="btn btn-primary btn-lg">Save Me</button>

              </form>
            </div>
          </div>
          <!-- /.box -->

  
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <script>$(document).ready(function() {
    $('#select').select2();
});</script>