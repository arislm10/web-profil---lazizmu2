<section class="content">
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kegiatan Kami</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('layout/tambah_kegiatan') ?>" enctype="multipart/form-data" method="post">
      <div class="modal-body">
      <div class="box-body">
                <div class="form-group">
                  <label for="kegiatan" class="col-sm- 5control-label">Nama Kegiatan</label>
                  <div class="col-sm-10">
                    <input type="text" name="kegiatan" class="form-control" id="inputEmail3" placeholder="nama kegiatan" required="">
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" name="deskripsi" rows="3" required placeholder="Enter ..."></textarea>
                </div>
                <br>
                <br>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
                    </div>
                </div>
                </div>

<div class="box box-default">
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Atur Kontak</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th> </th>
                  <th>Nama_contact</th>
                  <th>facebook</th>
                  <th>twitter</th>
                  <th>instagram</th>
                  <th>wa</th>
                  <th>no_telp</th>
                </tr>
                <tr>
                  <td><a href="<?= site_url('layout/ubahKontak') ?>" class="label label-warning">UBAH</a></td>
                  <td><?= $contact->nama_contact ?></td>
                  <td><?= $contact->fb ?></td>
                  <td><?= $contact->twitter ?></td>
                  <td><?= $contact->ig ?></td>
                  <td><?= $contact->whatsapp ?></td>
                  <td><?= $contact->no_telp ?></td>
                </tr>
              </tbody></table>
            </div>
            <div class="form-group">
                  <label>Deskrpsi</label>
                  <textarea class="form-control"  rows="3" placeholder="Enter ..." readonly><?= $contact->deskripsi ?></textarea>
                </div>
            <!-- /.box-body -->
          </div>
          </div>
        

        <div class="container">
          <div class="row">
          <div class="col-md-3">
              <form action="<?= site_url('layout/ubah_gambarHead') ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
              <input type="hidden" name="id" value="<?= $head_image->id_letak ?>">
                  <label for="exampleInputFile">Header Image</label>
                  <input type="file" name="gambar" id="exampleInputFile">
                  <img src="<?= base_url('uploads/').$head_image->gambar ?>" width="60" heigth="60">
                  <p class="help-block">di Atas Menu </p>
                  <button type="submit" class="btn btn-warning">Ganti</button>
                </div>
              </form>
            </div>
            <div class="col-md-3">
              <form action="<?= site_url('layout/ubah_gambarBackground') ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
              <input type="hidden" name="id" value="<?= $background_image->id_letak ?>">
                  <label for="exampleInputFile">Ubah background awal</label>
                  <input type="file" name="gambar" id="exampleInputFile">
                  <img src="<?= base_url('uploads/').$background_image->gambar ?>" width="60" heigth="60">
                  <p class="help-block">halaman awal </p>
                  <button type="submit" class="btn btn-warning">Ganti</button>
                </div>
              </form>
            </div>
            <div class="col-md-3">
              <form action="<?= site_url('layout/ubah_gambarEvent') ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
              <input type="hidden" name="id" value="<?= $event_image->id_letak ?>">
                  <label for="exampleInputFile">Ubah gambar kegiatan kami</label>
                  <input type="file" name="gambar" id="exampleInputFile">
                  <img src="<?= base_url('uploads/').$event_image->gambar ?>" width="60" heigth="60">
                  <p class="help-block">halaman awal di kegiatan kami </p>
                  <button type="submit" class="btn btn-warning">Ganti</button>
                </div>
              </form>
            </div>
            <div class="col-md-3">
            
            </div>
          </div>
          
        </div>
        <br>

        <br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
+Tambah Kegiatan
</button>
        <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th> </th>
                  <th>Nama Kegiatan</th>
                  <th>Deskripsi</th>

                </tr>
                <tr>
                  <?php foreach($laykeg as $lk ) {

                   ?>
                  <td><a href="<?= site_url('layout/edit_lihat_kegiatan/').$lk->id_layout ?>" class="label label-primary">EDIT</span></a><a href="<?= site_url('layout/hapus_lihat_kegiatan/').$lk->id_layout ?>" class="label label-danger">Hapus</span></a></td>
                  <td><?= $lk->nama_kegiatan ?></td>
                  <td><?= $lk->deskripsi ?></td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
          </div>
</div>
          <!-- /.box -->

<!-- /.row -->

</section>