<?php 

function cek_login() {

	$ci =& get_instance();
	if(!$ci->session->userdata('isLogin')) {
		$ci->session->set_flashdata('warning','GAGAL');
		redirect('admin/login');
	}
}
function cek_hakakses($akses = array()) {

	$ci = &get_instance();
	if(!in_array($ci->session->userdata('id_level'), $akses)) {
        $ci->session->set_flashdata('warning','anda tidak boleh mengakses modul tersebut');
        redirect('admin/home');
	}
}


?>