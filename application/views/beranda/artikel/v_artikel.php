
<section class="content">
      <div class="row">
        <div class="col-md-12">
<a href="<?= site_url('artikel/post_artikel') ?>" type="button" class="btn btn-primary"> 
Buat Berita
</a>
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong></strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 

          <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>label</th>
                  <th>gambar</th>
                  <th>keyword</th>
                  <th>Dari</th>
                  <th>Kategori</th>
                  <th>tanggal posting</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($artikel as $ak){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $ak->judul  ?></td>
                  <td><?= word_limiter($ak->content,30) ?></td>
                  <td><?= $ak->label  ?></td>
                  <td><img width="100" height="80" src="<?= base_url('gambar_artikel/').$ak->gambar ?>"></td>
                  <td><?= $ak->keyword ?></td>
                  <td><?= $ak->email ?></td>
                  <td><?= $ak->nama_kategori ?></td>
                  <td><?= $ak->tanggal_posting ?></td>
                  <td><a href="<?= site_url('artikel/edit_artikel/').$ak->id_artikel ?>" class="label label-warning">Edit</a>|| <a href="<?= site_url('artikel/hapus_artikel/').$ak->id_artikel ?>" class="label label-danger">Hapus</a> </td>
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>