
<section class="content">
      <div class="row">
        <div class="col-md-12">
        <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
        <form action="<?= site_url('program/update_tiketJariyah') ?>" enctype="multipart/form-data" method="post">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Program Tasyaruf</h3>
             
      <div class="form-group">
      <label for="usr">Judul:</label>
      <input type="text" class="form-control" id="usr" name="program" value="<?= $j->nama_program ?>">
      </div>
      <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
                <input type="file" class="form-control" name="gambar" value="<?= $j->gambar ?>">
                <img src="<?= base_url('uploads/').$j->gambar ?>" width="60" heigth="60">
                </div>
                </div>
      </div>
      <div class="box-body pad">
      <label for="usr">Deskripsi:</label>
                    <textarea id="editor1" name="deskripsi" rows="10" cols="80" style="visibility: hidden; display: none;"><?= $j->deskripsi; ?>
                    </textarea>
                    <br>
                    <button type="submit" class="btn btn-primary btn-lg">Save Me</button>

              </form>
            </div>
          </div>
          <!-- /.box -->

  
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>