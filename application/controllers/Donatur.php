<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donatur extends CI_Controller {


	public function index()
	{
        $data['content'] = 'donatur/add_donatur';
		$this->load->view('layout/clientTmp',$data);
    }
    public function view_donatur()
    {
        $data['content'] = 'donatur/v_donatur';
        $this->db->join('tb_metode','tb_donatur.id_metode= tb_metode.id_metode');
        $this->db->order_by("id_donatur", "desc");
        $data['donatur'] = $this->db->get('tb_donatur')->result();
        $this->load->view('layout/temp',$data);
  }
    public function hapus_donatur($id)
    {
       $this->db->delete('tb_donatur', array('id_donatur' => $id));
       $this->session->set_flashdata('error', 'Data Terhapus!'); 
        redirect('donatur/view_donatur');
  }
   public function cetak_donatur()
    {

         $this->db->join('tb_metode','tb_donatur.id_metode= tb_metode.id_metode');
         $this->db->order_by("id_donatur", "desc");
         $data['donatur'] = $this->db->get('tb_donatur')->result();
         $this->load->view('donatur/cetak_donatur',$data);
  }
    public function tambah_donatur()
	{
		$nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $pekerjaan = $this->input->post('pekerjaan');
        $penghasilan = $this->input->post('penghasilan');
        $donasi = $this->input->post('donasi');

        $don=implode(",",$donasi);
        $tgl = $this->input->post('tanggal_donasi');
        $metode = $this->input->post('metode');
        $jumlah = $this->input->post('jumlah');
        $data = array(

        	'nama_lengkap' => $nama,
        	'alamat'=> $alamat,
        	'telp'=> $telp,
        	'email'=> $email,
        	'pekerjaan' => $pekerjaan,
        	'jumlah_penghasilan' => $penghasilan,
        	'jumlah' => $jumlah,
        	'id_metode'=> $metode,
        	'tanggal_donasi'=>$tgl,
        	'donasi' => $don

        );
 
       $this->db->insert('tb_donatur',$data);
           $this->session->set_flashdata('success', 'Terimakasih Telah Mennjadi Donatur, Data Telah Kami Simpan'); 
       redirect('donatur');
    }
       public function edit_donatur()
	{
		$nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $pekerjaan = $this->input->post('pekerjaan');
        $penghasilan = $this->input->post('penghasilan');
        $donasi = $this->input->post('infaq');
        $tgl = $this->input->post('tanggal_donasi');
        $metode = $this->input->post('metode');
        $jumlah = $this->input->post('jumlah');

        $data = array(

        	'nama_lengkap' => $nama,
        	'alamat'=> $alamat,
        	'telp'=> $telp,
        	'email'=> $email,
        	'pekerjaan' => $pekerjaan,
        	'jumlah_penghasilan' => $penghasilan,
        	'jumlah' => $jumlah,
        	'id_metode'=> $metode,
        	'tanggal_donasi'=>$tgl,
        	'donasi' => $donasi
        );
       $this->db->insert('tb_donatur',$data);
       redirect('donatur');
    }

    public function delete($id) {

    }
}