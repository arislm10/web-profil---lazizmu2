
<section class="content">
      <div class="row">
        <div class="col-md-12">
        <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
   <button type="button" class="btn btn-primary btn-sm">Print</button>
        <form action="<?= site_url('Jaringan/update_jaringan') ?>" method="post">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Jaringan</h3>
             
      <div class="form-group">
      <label for="usr">Judul:</label>
      <input type="text" class="form-control" id="usr" name="title" value="<?= $jaringan->title ?>">
      </div>
      <div class="form-group">
      <label for="usr">Keyword:</label>
      <input type="text" class="form-control" id="usr" name="keyword" value="<?= $jaringan->keyword ?>">
      </div>
      <input type="hidden" name="date" value="<?=date("Y-m-d")  ?>">
      </div>
      <div class="box-body pad">
                    <textarea id="editor1" name="content" rows="10" cols="80" style="visibility: hidden; display: none;"><?= $jaringan->content; ?>
                    </textarea>
                    <br>
                    <button type="submit" class="btn btn-primary btn-lg">Save Me</button>

              </form>

            </div>
          </div>
          <!-- /.box -->

  
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>