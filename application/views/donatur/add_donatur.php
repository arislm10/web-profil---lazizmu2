<br><br><br><br>
<section class="content">
   <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Donatur</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?= site_url('donatur/tambah_donatur') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" name="nama" class="form-control" value="" placeholder="nama Lengkap" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <input type="text" name="alamat" class="form-control" value="" required="" placeholder="Alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nomor Telepon</label>
                  <div class="col-sm-10">
                    <input type="text" name="telp" class="form-control" value="" required="" placeholder="Nomor Telepon">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" value="" required="" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pekerjaan </label>
                  <div class="col-sm-10">
                    <input type="text" name="pekerjaan" class="form-control" value="" required="" placeholder="Pekerjaan">
                  </div>
                </div>
                    <div class="form-group">
                  <label for="inputEmail3" class="col-sm-5 control-label">Penghasilan Perbulan </label>
                  <div class="col-sm-10">
                    <input type="int" name="penghasilan" class="form-control" required="" value="" placeholder="Penghasilan Perbulan">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-5 control-label">Donasi </label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="donasi[]" value="Zakat" >Zakat 
                    <input type="checkbox" name="donasi[]" value="Infaq dan Sodakoh">Infaq dan Sodakoh
                    <input type="checkbox" name="donasi[]" value="Program">Program
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label" required="">Tanggal Donasi </label>
                  <div class="col-sm-10">
                    <input type="date" name="tanggal_donasi" class="form-control" value="" placeholder="kegiatan" required="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Metode Donasi</label>
                  <div class="col-sm-10">
                 <select class="form-control" name="metode" id="exampleFormControlSelect1" required="">
                      <option value="1">Transfer ke rekening Lazismu D.I Yogyakarta</option>
                      <option value="2">Layanan jemput Lazismu</option>
                 </select>
                  </div>
                </div>
                <input type="hidden" name="id" value="" required="">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Donasi</label>
                  <div class="col-sm-10">
                  <textarea name="jumlah" class="form-control" required="" rows="3" placeholder="Jumlah Donasi"></textarea>
                  <br>
                   <button type="submit" align="left" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
 
              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>