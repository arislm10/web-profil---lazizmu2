<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Lazizmu Umbulharjo</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url()  ?>uploads/logo1.png" rel="icon">
  <link href="<?php echo base_url()  ?>source/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url()  ?>source/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url()  ?>source/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url()  ?>source/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url()  ?>source/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url()  ?>source/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url()  ?>source/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url()  ?>source/css/style.css" rel="stylesheet">
  <script src="<?php echo base_url()  ?>source/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url()  ?>source/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url()  ?>source/js/main.js"></script>
  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<?php 
        $ip       = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Ymd"); // Mendapatkan tanggal sekarang
        $waktu   = time(); // 
        // Mencek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini 
        $s = $this->db->query("SELECT * FROM statistik WHERE ip='$ip' AND tanggal='$tanggal'")->num_rows();
        // Kalau belum ada, simpan data user tersebut ke database
        if($s == 0){
            $this->db->query("INSERT INTO statistik(ip, tanggal, hits, online) VALUES('$ip','$tanggal','1','$waktu')");
        } 
        else{
            $this->db->query("UPDATE statistik SET hits=hits+1, online='$waktu' WHERE ip='$ip' AND tanggal='$tanggal'");
        } ?>

        
    <script type="text/javascript">
    if ($('.main-nav').length) {
    var $mobile_nav = $('.main-nav').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="fa fa-bars"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('.mobile-nav-overly').toggle();
    });
    
    $(document).on('click', '.drop-down a', function(e) {
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });

    $(document).click(function(e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }

})(jQuery);
</script>