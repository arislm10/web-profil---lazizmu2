<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/comment.css">
<br>

<br>

<br>
<div class="main-page">
    
   <!-- top-nav -->
   <div class="top-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6"></div>
            </div>
        </div>   
   </div>
   
   <!-- main-banner -->
    <div class="main-banner">
        <div class="container">
            <img src="https://www.sga.uni-hamburg.de/7850180/chuttersnap-1200x300-f576e44a846d72766dc57e1a343ad4c9a1281798.jpg" class="img-responsive">
        </div>
    </div>
    
    <!-- post-widgets -->
    <div class="post-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <ol class="breadcrumb">
                      <li><span>Post Artikel</span></li>
                    </ol>
                    <div class="page">
                        <h3><?= $artikel->judul ?></h3>
                        <div class="details">
                            <ul>
                                <li><?= $artikel->email ?></li>
                                <li><?= $artikel->tanggal_posting ?></li>
                                <li><?= $artikel->nama_kategori ?></li>
                            </ul>
                        </div>
                        <div class="content">
                            <img src="<?= base_url('gambar_artikel/').$artikel->gambar ?>" width="100%">

                            <p><?= $artikel->content ?></p>
                        </div>
                            <br>
                            <br>
                            <br>
                       <p>Label :</p>
                        <?php 
                        $str = $artikel->label;
                        $label = explode(" ",$str);
                        foreach($label as $key => $value){
                        ?>
                        <a style="color:white" class="tag"><?php echo $value; ?></a>
                        <?php } ?>
                        <div class="form">
                        <hr>
                        <br>

		    <div class="blog-comment">
				<h3 class="text-success">Comments</h3>
                <hr/>

                <?php $id= $this->uri->segment(3); $komentar= $this->db->get_where('tb_komentar',array('id_artikel'=> $id)); 
                
                foreach($komentar->result() as $k) {?>
				<ul class="comments">
				<li class="clearfix">
				  <img src="<?php echo base_url() ?>assets\dist\img\avatar5.png" class="avatar" alt="">
				  <div class="post-comments">
				      <p class="meta"><?= $k->date ?> <a href="#"><?= $k->nama  ?></a> says : <i class="pull-right"><a href="#"></a></i></p>
				      <p>
				          <?= $k->komentar; ?>
				      </p>
				  </div>
				</li>
				</ul>
                <?php } ?>
			</div>

                        <div id="sendmessage">Tambah Komentar</div>
                          <div id="errormessage"></div>
                            <form action="<?= site_url('komentar/proses_komentar') ?>" method="post">
                                <div class="form-row">
                                <div class="form-group col-lg-6">
                                    <input required type="text" name="nama" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <input required type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
                                    <div class="validation"></div>
                                </div>
                                </div>
                                <input type="hidden" name="date" value="<?= date('Y-m-d') ?>">
                                <input type="hidden" name="artikel" value="<?= $this->uri->segment(3) ?>">
                                <div class="form-group">
                                <textarea required class="form-control" name="komentar" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="komentar"></textarea>
                                <div class="validation"></div>
                                </div>
                                <div><input type="submit" class="btn btn-primary" title="Send Message" value="Komentar"></div>
                            </form>
                        </div>
                        
                    </div> 
                    
                </div>
                   
                <div class="col-md-4">
                    
                <div class="widgets">
                        <h3>Search</h3>
                        <form action="<?= site_url('artikel/cari_artikel') ?>" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="kata_kunci" placeholder="Search term...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default" type="button">search</button>
                            </span>
                        </div>
                        </form>
                    </div>
                    <div class="widgets">
                        <h3>Statistik</h3>
                         <?php
                        echo "<li>User Online : $online</li>
                        <li>Today Visitor : $pengunjung</li>
                        <li>Hits hari ini : $hits[total]</li>
                        <li>Total pengunjung : $total[total]</li>";
                        ?>
                    </div>
                    <div class="widgets">
                        <h3>Categori</h3>
                        <ul>
                        <?php foreach($kategori as $val){
                            ?>
                            <li><a href="<?= site_url('beranda/artikel_kategori/').$val->id_kategori ?>"><?= $val->nama_kategori ?></a></li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="widgets" id="datepicker">
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div><!-- main-page -->
<script type="text/javascript">
    $('#datepicker').datepicker({});
</script>