<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jaringan extends CI_Controller {


	public function index()
	{
        $data['content'] = 'jaringan/v_jaringan';
        $data['jaringan'] = $this->db->get('tb_jaringan')->row();
		$this->load->view('layout/temp',$data);
    }
    public function update_jaringan() {
        
        $key = $this->input->post('keyword');
        $judul = $this->input->post('title');
        $isi = $this->input->post('content');
        $date = $this->input->post('date');
        $data = array(
            
            'keyword' => $key,
            'title' => $judul,
            'content' => $isi,
            'tgl_update'   => $date
        );
        if(isset($data)){
            $this->db->where('id_jaringan', 1);
            $u = $this->db->update('tb_jaringan', $data);
            if($u == 1) {
                $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                redirect('jaringan');
            }
        }else {
            $this->session->set_flashdata('error', 'Data gagal Masuk'); 
            redirect('jaringan');
        }

    }
    public function cetak(){

        $data['content'] = 'jaringan/cetak_jaringan';
        $data['jaringan'] = $this->db->get('tb_jaringan')->row();
        $this->load->view('jaringan/cetak_jaringan',$data);
    }
}
