<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {


	public function index()
	{
        $data['content'] = 'program/v_programP';
        $data['p'] = $this->db->get_where('tb_program',array('id_program' => 2))->row();
		$this->load->view('layout/temp',$data);
    }
    public function program_tasyaruf()
	{
        $data['content'] = 'program/v_programT';
        $data['t'] = $this->db->get_where('tb_program',array('id_program' => 1))->row();
		$this->load->view('layout/temp',$data);
    }
    public function update_programP() {
        
        $program = $this->input->post('program');
        $deskripsi = $this->input->post('deskripsi');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|svg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }

        $data = array(
            'nama_program' => $program,
            'deskripsi' => $deskripsi
        );

        if (isset($image)) {
            $data['gambar'] = $image;  
        }
        

        if(isset($data)){
           $this->db->where('id_program', 2);
           $update = $this->db->update('tb_program', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('program');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('program');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('program');
        }
    }

    public function update_programT() {
        
        $program = $this->input->post('program');
        $deskripsi = $this->input->post('deskripsi');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|svg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'nama_program' => $program,
            'deskripsi' => $deskripsi
        );
        if (isset($image)) {
            $data['gambar'] = $image;  
        }
        if(isset($data)){
           $this->db->where('id_program', 1);
           $update = $this->db->update('tb_program', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('program/program_tasyaruf');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('program/program_tasyaruf');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('program/program_tasyaruf');
        }
    }
     public function program_tiketJariyah()
    {
        $data['content'] = 'program/v_tiketJariyah';
        $data['j'] = $this->db->get_where('tb_program',array('id_program' => 3))->row();
        $this->load->view('layout/temp',$data);
    }
      public function update_tiketJariyah() {
        
        $program = $this->input->post('program');
        $deskripsi = $this->input->post('deskripsi');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|svg|png';
        $config['overwrite']            = true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'nama_program' => $program,
            'deskripsi' => $deskripsi
        );
        if (isset($image)) {
            $data['gambar'] = $image;  
        }
        if(isset($data)){
           $this->db->where('id_program', 3);
           $update = $this->db->update('tb_program', $data);

                if($update == TRUE){
                    $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                    redirect('program/program_tiketJariyah');
                }else {
                    $this->session->set_flashdata('error', 'Data gagal diubah'); 
                    redirect('program/program_tiketJariyah');
                }
        }else {
            $this->session->set_flashdata('error', 'Data Error'); 
                    redirect('program/program_tiketJariyah');
        }
    }
}
