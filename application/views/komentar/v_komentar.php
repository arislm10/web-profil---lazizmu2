
<section class="content">
      <div class="row">
        <div class="col-md-12">
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>dari</th>
                  <th>judul</th>
                  <th>email</th>
                  <th>komentar</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($komentar as $g){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $g->nama  ?></td>
                  <td><?= $g->judul ?></td>
                  <td><?= $g->email ?></td>
                  <td><?= $g->komentar ?></td>
                  
                  <td><a href="<?= site_url('admin/hapus_komentar/').$g->id_komentar ?>" class="btn btn-danger">Hapus</a> </td>
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>