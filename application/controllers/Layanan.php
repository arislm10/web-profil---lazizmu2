<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {


	public function index()
	{
        $data['content'] = 'layanan/v_PanduanDonasi';
        $data['panduan'] = $this->db->get('tb_layanan')->row();
		$this->load->view('layout/temp',$data);
    }
    public function update_panduan(){

        $key = $this->input->post('keyword');
        $judul = $this->input->post('title');
        $isi = $this->input->post('content');
        $date = $this->input->post('date');
        $data = array(
            
            'keyword' => $key,
            'title' => $judul,
            'content' => $isi,
            'tanggal_update'   => $date
        );
        if(isset($data)){
            $this->db->where('id_layanan', 1);
            $u = $this->db->update('tb_layanan', $data);
            if($u == 1) {
                $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                redirect('layanan');
            }
        }else {
            $this->session->set_flashdata('error', 'Data gagal Masuk'); 
            redirect('layanan');
    }
    
}
}
