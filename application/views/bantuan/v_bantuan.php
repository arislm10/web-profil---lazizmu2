
<section class="content">
      <div class="row">
        <div class="col-md-12">
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong></strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
<a target="_blank" href="<?= site_url('bantuan/cetak_bantuan'); ?>" class="btn btn-primary">Cetak Data</a>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Alamat</th>
                  <th>No Hp</th>
                  <th>Pekerjaan</th>
                  <th>Penghasilan</th>
                  <th>Jenis Pengajuan</th>
                  <th>KK</th>
                  <th>KTP</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($bantuan as $g){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $g->nama_lengkap  ?></td>
                  <td><?= $g->alamat ?></td>
                   <td><?=$g->telp ?></td>
                  <td><?= $g->pekerjaan  ?></td>
                  <td><?= $g->penghasilan ?></td>
                   <td><?= $g->jenis_pengajuan ?></td>
                  <td> <img src="<?= base_url('gambar_artikel/').$g->kk ?>" width="150" heigth="100"></td>
                  <td> <img src="<?= base_url('gambar_artikel/').$g->ktp ?>" width="150" heigth="100"></td>
                  <td> <a href="<?= site_url('bantuan/hapus_bantuan/').$g->id_bantuan ?>" class="btn btn-danger">Hapus</a> </td>
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>