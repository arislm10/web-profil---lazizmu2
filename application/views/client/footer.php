<?php   $contact= $this->db->get('tb_contact')->row(); ?>
<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Lazizmu</h3>
            <p><?= $contact->deskripsi ?></p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="<?= site_url('beranda') ?>">Home</a></li>
              <li><a href="<?= site_url('beranda/latar_belakang')?>">Latar Belakang</a></li>
              <li><a href="<?= site_url('beranda/donasi') ?>">Donasi</a></li>
              <li><a href="<?= site_url('beranda/artikelku') ?>">Berita</a></li>
              <li><a href="<?= site_url('beranda/jaringan') ?>">Jaringan</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Kontak Kami</h4>
            <p>
            Jl. Glagahsari No.136 <br>
            Warungboto, Umbulharjo, Kota Yogyakarta<br>
            Daerah Istimewa Yogyakarta 55164 <br>
              <i class="fa fa-whatsapp"></i>&nbsp;<?= $contact->whatsapp ?><br>
              <i class="fa fa-phone"></i>&nbsp;<?= $contact->no_telp ?><br>
              
            </p>
       
            <div class="social-links">
              <a href="<?= $contact->twitter ?>" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="<?= $contact->fb ?>" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="<?= $contact->ig ?>" class="instagram"><i class="fa fa-instagram"></i></a>
          
            </div>

          </div>

    

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url()  ?>source/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url()  ?>source/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url()  ?>source/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url()  ?>source/js/main.js"></script>

 

</body>
</html>
