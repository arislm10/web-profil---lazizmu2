<section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">EDIT Daftar xKegiatan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?= site_url('layout/ubah_kegiatan') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Kegiatan</label>
                  <div class="col-sm-10">
                    <input type="text" name="kegiatan" class="form-control" value="<?= $keg->nama_kegiatan ?>" placeholder="kegiatan">
                  </div>
                </div>
                <input type="hidden" name="id" value="<?= $keg->id_layout ?>">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                  <textarea name="deskripsi" class="form-control" rows="3" placeholder="Enter ..."><?= $keg->deskripsi ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Save Me</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>