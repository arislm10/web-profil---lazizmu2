
<section class="content">
      <div class="row">
        <div class="col-md-12">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
+Tambah Galeri
</button>
<?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('galeri/tambah_galeri') ?>" enctype="multipart/form-data" method="post">
      <div class="modal-body">
      <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">judul</label>
                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="inputEmail3" placeholder="title">
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <input type="text" name="deskripsi" class="form-control" id="inputPassword3" placeholder="Deskripsi">
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>

                <div class="col-sm-10">
                    <input type="file" class="form-control" name="gambar">
                </div>
                </div>

                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
                    </div>
                </div>
                </div>
 
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Deskripsi</th>
                  <th>gambar</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($gambar->result() as $g){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $g->title  ?></td>
                  <td><?= $g->deskripsi ?></td>
                  <td><img width="350" height="180" src="<?= base_url('uploads/').$g->gambar ?>"></td>
                  <td><a href="<?= site_url('galeri/edit_gambar/').$g->id_galeri ?>" class="btn btn-warning">Edit</a>|| <a href="<?= site_url('galeri/hapus_gambar/').$g->id_galeri ?>" class="btn btn-danger">Hapus</a> </td>
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>