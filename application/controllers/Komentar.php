<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komentar extends CI_Controller {


	public function proses_komentar()
	{ 	
        $email = $this->input->post('email');
        $nama = $this->input->post('nama');
        $komentar = $this->input->post('komentar');
        $id = $this->input->post('artikel');
        $date = $this->input->post('date');
        $data = array(

            'email' => $email,
            'nama' => $nama,
            'date' => $date,
            'komentar' => $komentar,
            'id_artikel'=> $id
        );
        // print_r($data);
        $this->db->insert('tb_komentar',$data);
        redirect('artikel/lihat_artikel/'.$id);
    }
}