<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	function __construct(){

        parent::__construct();
   		
    }

	public function index()
	{ 	
		$this->load->helper('text');
		$data['content'] = 'client/content';
		$data['kegiatan'] = $this->db->get('tb_layout_kegiatan')->result();
		$this->load->view('layout/clientTmp',$data);
	}
	public function konfirmasi() {

		$data['content'] = 'konfirmasi/add_konfirmasi';
		$this->load->view('layout/clientTmp',$data);
	}
	public function konfirmasi_proses() {

		$email = $this->input->post('email');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('bukti')) {
            $image = $this->upload->data("file_name");
        }

        $data = array(
        		'email' => $email,
        		'bukti_upload' => $image
        );

        $this->db->insert('tb_konfrimasi',$data);
            $this->session->set_flashdata('success', 'Terimakasih Telah Melakuakn Konfirmasi'); 
        redirect('beranda/konfirmasi');
	}
	public function artikelku($from = 0)
	{ 	
		$this->load->library('pagination');
		$this->load->helper('text');
		$this->load->model('model_utama');
		$data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
		$data['total']  = $this->model_utama->totalpengunjung()->row_array();
		$data['hits']             = $this->model_utama->hits()->row_array();
		$data['totalhits']        = $this->model_utama->totalhits()->row_array();
		$data['online'] = $this->model_utama->pengunjungonline()->num_rows();
		$data['content'] = 'beranda/artikel/artikel';
		$data['kategori'] = $this->db->get('tb_kategori')->result();
		$jumlah_data = $this->db->get('tb_artikel')->num_rows();

		$config['base_url'] = base_url().'index.php/beranda/artikelku';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 4;
		$config['num_links'] = 2; 
 		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();	
		$this->db->select('*');
		$this->db->order_by("tanggal_posting", "desc");
		$this->db->join('tb_kategori','tb_artikel.id_kategori = tb_kategori.id_kategori');
		$data['artikel'] = $this->db->get('tb_artikel',$config['per_page'],$from)->result_array();

		$this->load->view('layout/clientTmp',$data);
	}
	public function artikel_kategori($kategori)
	{ 	
		$this->load->library('pagination');
		$this->load->helper('text');
		$this->load->model('model_utama');
		$data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
		$data['total']  = $this->model_utama->totalpengunjung()->row_array();
		$data['hits']             = $this->model_utama->hits()->row_array();
		$data['totalhits']        = $this->model_utama->totalhits()->row_array();
		$data['online'] = $this->model_utama->pengunjungonline()->num_rows();
		$data['content'] = 'beranda/artikel/artikel';
		$data['kategori'] = $this->db->get('tb_kategori')->result();
		$jumlah_data = $this->db->get_where('tb_artikel',array('id_kategori'=> $kategori))->num_rows();

		$config['base_url'] = base_url().'index.php/beranda/artikelku';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 4;
		$config['num_links'] = 2; 
 		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['pagination'] = $this->pagination->create_links();	
		$this->db->select('*');
		$this->db->from('tb_artikel');
		$this->db->join('tb_kategori','tb_artikel.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_artikel.id_kategori',$kategori);
		$this->db->order_by("tanggal_posting", "desc");
		$data['artikel'] = $this->db->get()->result_array();
		// print_r($data['artikel']);
		$this->load->view('layout/clientTmp',$data);
	}
	public function Galeri()
	{  

		$data['content'] = 'beranda/Galeri';
		$data['galeri'] = $this->db->get_where('tb_galeri')->result();
		$this->load->view('layout/clientTmp',$data);
	}
	public function kebijakan_strategis()
	{  

		$data['content'] = 'beranda/profile/kebijakan_strategis';
		$data['ks'] = $this->db->get_where('tb_profile',array('id_profile' => 4))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function visi_misi()
	{  

		$data['content'] = 'beranda/profile/visi_misi';
		$data['visi_misi'] = $this->db->get_where('tb_profile',array('id_profile' => 1))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function latar_belakang()
	{  

		$data['content'] = 'beranda/profile/latar_belakang';
		$data['latar_belakang'] = $this->db->get_where('tb_profile',array('id_profile' => 2))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function struktur_organisasi()
	{  

		$data['content'] = 'beranda/profile/struktur_organisasi';
		$data['st'] = $this->db->get_where('tb_profile',array('id_profile' => 3))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function donasi()
	{  
		$data['content'] = 'beranda/donasi';
		$data['donasi'] = $this->db->get('tb_donasi')->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function panduan_donasi()
	{  
		$data['content'] = 'beranda/layanan';
		$data['layanan'] = $this->db->get('tb_layanan')->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function jaringan()
	{  
		$data['content'] = 'beranda/jaringan';
		$data['jaringan'] = $this->db->get('tb_jaringan')->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function program_pentasyarufan()
	{  
		$data['content'] = 'beranda/program/p_petasyarufan';
		$data['pt'] = $this->db->get_where('tb_program',array('id_program' => 1))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function program_penghimpunan()
	{  
		$data['content'] = 'beranda/program/p_penghimpunan';
		$data['pp'] = $this->db->get_where('tb_program',array('id_program' => 2))->row();
		$this->load->view('layout/clientTmp',$data);
	}
	public function tiket_jariyah()
	{  
		$data['content'] = 'beranda/program/tiket_jariyah';
		$data['tj'] = $this->db->get_where('tb_program',array('id_program' => 3))->row();
		$this->load->view('layout/clientTmp',$data);
	}
}
