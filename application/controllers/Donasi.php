<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donasi extends CI_Controller {


	public function index()
	{
        $data['content'] = 'donasi/v_donasi';
		$data['donasi'] = $this->db->get('tb_donasi')->row();
		$this->load->view('layout/temp',$data);
    }
    public function update_donasi()
	{
        $key = $this->input->post('keyword');
        $judul = $this->input->post('title');
        $isi = $this->input->post('content');
        $date = $this->input->post('date');
        $data = array(
            
            'keyword' => $key,
            'title' => $judul,
            'content' => $isi,
            'tanggal_update'   => $date
        );
        if(isset($data)){
            $this->db->where('id_donasi', 1);
            $u = $this->db->update('tb_donasi', $data);
            if($u == 1) {
                $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                redirect('donasi');
            }
        }else {
            $this->session->set_flashdata('error', 'Data gagal Masuk'); 
            redirect('donasi');
        }
	}
}
    