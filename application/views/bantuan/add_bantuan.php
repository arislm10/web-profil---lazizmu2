<br><br><br><br>
<section class="content">
   <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Bantuan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?= site_url('bantuan/tambah_bantuan') ?>"enctype="multipart/form-data" >
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" name="nama" class="form-control" value="" placeholder="nama Lengkap" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <input type="text" name="alamat" class="form-control" value="" placeholder="Alamat" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nomor Telepon</label>
                  <div class="col-sm-10">
                    <input type="text" name="telp" class="form-control" value="" placeholder="Nomor Telepon" required="">
                  </div>
                </div>
 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pekerjaan </label>
                  <div class="col-sm-10">
                    <input type="text" name="pekerjaan" class="form-control" value="" placeholder="Pekerjaan" required="">
                  </div>
                </div>
                    <div class="form-group">
                  <label for="inputEmail3" class="col-sm-5 control-label">Jumlah Penghasilan </label>
                  <div class="col-sm-10">
                    <input type="int" name="penghasilan" class="form-control" value="" placeholder="Penghasilan Perbulan" required="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-5 control-label">Jenis Pengajuan </label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="pengajuan[]" value="Bantuan/Beasiswa">Bantuan/Beasiswa
                    <input type="checkbox" name="pengajuan[]" value="pendidikan">Pendidikan
                    <input type="checkbox" name="pengajuan[]" value="Bantuan Modal Usaha ">Bantuan Modal Usaha
                    <input type="checkbox" name="pengajuan[]" value="Fakir Miskin">Fakir Miskin
                    <input type="checkbox" name="pengajuan[]" value="Ibnu Sabil">Ibnu sabil
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kartu Keluarga </label>
                  <div class="col-sm-10">
                    <input type="file" name="kk" class="form-control" value="" placeholder="kegiatan" required="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">KTP</label>
                  <div class="col-sm-10">
                    <input type="file" name="ktp" class="form-control" value="" placeholder="kegiatan" required="">
                  </div>
                  <br>
                  <button type="submit" align="left" class="btn btn-primary">Simpan</button>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>