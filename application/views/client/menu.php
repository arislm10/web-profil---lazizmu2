

<body>

<!--==========================
Header
============================-->
<header id="header" class="fixed-top">
  <div class="container">

    <div class="logo float-left">
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
      <?php $image=$this->db->get_where('tb_layout_gambar',array('id_letak' => 1))->row() ?>
      <a href="<?= site_url('beranda') ?>" class="scrollto"><img  src="<?= base_url('uploads/').$image->gambar ?>" width="100"></a>
    </div>

    <nav class="main-nav float-right d-none d-lg-block">
      <ul>
        <li class="active"><a href="<?= site_url('beranda') ?>">Home</a></li>
        <li class="drop-down"><a href="#">Profile</a>
          <ul>
            <li><a href="<?= site_url('beranda/struktur_organisasi')?>">Struktur Organisasi</a></li>
            <li><a href="<?= site_url('beranda/latar_belakang')?>" >Latar Belakang</a> </li>
            <li><a href="<?= site_url('beranda/visi_misi')?>">Visi Misi Kami</a></li>
            <li><a href="<?= site_url('beranda/kebijakan_strategis') ?>">Kebijakan Strategis</a></li>
            </li>
          </ul>
        </li>
        <li class="drop-down"><a href="#">Program</a>
          <ul>
            <li><a href="<?= site_url('beranda/program_penghimpunan') ?>">Program Penghimpunan</a></li>
            <li><a href="<?= site_url('beranda/program_pentasyarufan') ?>">Program Petasyarufan</a> </li>
              <li><a href="<?= site_url('beranda/tiket_jariyah') ?>">Tiket Jariyah</a> </li>
            </li>
          </ul>
        </li>
        <li><a href="<?= site_url('beranda/jaringan') ?>">Jaringan</a></li>
        <li><a href="<?= site_url('beranda/donasi') ?>">Donasi</a></li>
        <li><a href="<?= site_url('beranda/artikelku') ?>">Berita</a></li>
        <li><a href="<?= site_url('beranda/galeri') ?>">Galery</a></li>
        <li class="drop-down"><a href="#">Layanan</a>
          <ul>
            <li><a href="<?= site_url('beranda/panduan_donasi') ?>">Panduan Donasi</a></li>
            <li><a  href="<?= site_url('Donatur') ?>">Pendaftaran Donatur</a> </li>
            <li><a  href="<?= site_url('Bantuan') ?>">Pendaftaran Bantuan</a></li>
            <li><a  href="<?= site_url('beranda/konfirmasi') ?>">Konfirmasi Donasi</a> </li>
            <li><a target="_blank" href="http://online.lazismu.org/index.php?r=kalkulatorZakat">Kalkulator Zakat</a> </li>
            </li>
          </ul>
        </li>
      </ul>
    </nav><!-- .main-nav -->
    
  </div>
</header><!-- #header -->

<!--==========================
  Intro Section
============================-->