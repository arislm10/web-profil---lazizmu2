
<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jumlah</th>
                  <th>Tanggal</th>
             
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($st->result() as $s){
                  ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $s->hits  ?></td>
                  <td><?= $s->tanggal ?></td>
                  
                 
                </tr>
                  <?php } ?>
               
              
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
</section>
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>