<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	
function login(){

		$this->load->view('login');
    }
    
    public function home() {
		$this->load->model('model_utama');
		$data['content'] = 'dashboard';
		$data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
		$this->load->view('layout/temp',$data);
    }
     public function konfirmasi() {
        $data['content'] = 'konfirmasi/v_konfirmasi';
        $data['konfirmasi'] = $this->db->get('tb_konfrimasi');
		$this->load->view('layout/temp',$data);
    }
     public function cetak_konfirmasi() {
       
    $data['konfirmasi'] = $this->db->get('tb_konfrimasi');
		$this->load->view('konfirmasi/cetak_konfirmasi',$data);
    }
	function aksi_login(){
        $this->load->model('M_login');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$where = array(
			'email' => $email,
			'password' => $password
			);
        $cek = $this->M_login->cek_login($where)->num_rows();
        print_r($cek);
		if($cek > 0){
            $level = $this->db->get_where('tb_admin',$where)->row();
			$data_session = array(
				'isLogin' => TRUE,
                'id_level' => $level->id_level,
                'email' => $email
				);
 
			$this->session->set_userdata($data_session);
			$this->session->set_flashdata('info', 'Selamat Datang Kembali '.$data_session['email'].'!'); 
			redirect('admin/home');
 
		}else{
			$this->session->set_flashdata('error', 'Username dan Password Salah Silahkan Coba Kembali'); 
			redirect('admin/login');
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect('admin/login');
	}
	public function lihat_komentar(){
     $data['content'] = 'komentar/v_komentar';
        $this->db->join('tb_artikel','tb_komentar.id_artikel=tb_artikel.id_artikel');
		$data['komentar']=$this->db->get('tb_komentar')->result();
		$this->load->view('layout/temp',$data);
	}
	public function hapus_komentar($id){
		$this->db->delete('tb_komentar', array('id_komentar' => $id));
		$this->session->set_flashdata('success', 'Komentar Terhapus!'); 
		redirect('admin/lihat_komentar');
	 }

	 function email() {
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "arispw4@gmail.com";
		$config['smtp_pass'] = "Parawali2800";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		
		
		$ci->email->initialize($config);

		$ci->email->from('arispw4@gmail.com', 'Your Name');
		$list = array('chanelyt78@gmail.com');
		$ci->email->to($list);
		$ci->email->subject('judul email');
		$ci->email->message('isi email');
		if ($this->email->send()) {
				echo 'Email sent.';
		} else {
				show_error($this->email->print_debugger());
		}
}
public function statistik() {
		$data['content'] = 'statistik';
		$this->db->order_by("id_statistik", "desc");
		$data['st'] = $this->db->get('statistik');
		$this->load->view('layout/temp',$data);
	}
}