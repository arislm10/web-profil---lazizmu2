<br>

<br>
<section id="portfolio" class="clearfix">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Foto - Foto Kami</h3>
        </header>

        <div class="row">

        </div>

        <div class="row portfolio-container">
<?php foreach($galeri as $foto){

 ?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('uploads/').$foto->gambar  ?>" class="img-fluid" alt="">
              <div  class="portfolio-info">
                <span><a style="color:white" href="#"><?= $foto->title ?></a></span>
                <p><?= $foto->deskripsi ?></p>
                <div>
                  <a href="<?php echo base_url('uploads/').$foto->gambar  ?>" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

<?php } ?>

        </div>

      </div>
    </section><!-- #portfolio -->