<section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">UBAH KONTAK</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"  enctype="multipart/form-data" action="<?= site_url('galeri/edit_proses') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" value="<?= $gambar->title ?>" placeholder="kegiatan">
                  </div>
                  </div>
                  <input type="hidden" name="id" value="<?= $gambar->id_galeri ?>">
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <input type="text" name="deskripsi" class="form-control" value="<?= $gambar->deskripsi ?>" placeholder="kegiatan">
                  </div>
                  </div>
                <div class="form-group">
                <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                <div class="col-sm-10">
                <input type="file" class="form-control" name="gambar">
                <img src="<?= base_url('uploads/').$gambar->gambar ?>" width="60" heigth="60">
                </div>
                </div>
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>