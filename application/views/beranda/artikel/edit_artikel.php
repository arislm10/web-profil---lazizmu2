
<section class="content">
      <div class="row">
        <div class="col-md-12">
        <?php if($this->session->flashdata('success')){ ?>  
     <div class="alert alert-success">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('error')){ ?>  
     <div class="alert alert-danger">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('warning')){ ?>  
     <div class="alert alert-warning">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>  
     </div>  
   <?php } else if($this->session->flashdata('info')){ ?>  
     <div class="alert alert-info">  
       <a href="#" class="close" data-dismiss="alert">&times;</a>  
       <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>  
     </div>  
   <?php } ?> 
        <form action="<?= site_url('artikel/edit_artikel_proses') ?>" enctype="multipart/form-data" method="post">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">EDIT BERITA</h3>
             <input type="hidden" name="id" value="<?= $artikel->id_artikel?>">
      <div class="form-group">
      <label for="usr">Judul:</label>
      <input type="text" class="form-control" id="usr" name="title" value="<?= $artikel->judul  ?>" required="">
      </div>
      <div class="form-group">
                  <label>Kategori</label>
                  <select name="kategori" class="form-control" required="">
                 <option <?php if('1' == $artikel->id_kategori) { echo "selected"; } ?> value="1">Berita</option>
                 <option  <?php if('2' == $artikel->id_kategori) { echo "selected"; } ?> value="2">Kisah Inspirasi</option>

                  </select>
                </div>
                <?php
                $a = $artikel->label; 
                $array = explode(" ",$a);
                ?>
      <input type="hidden" name="email" value="<?= $this->session->userdata('email'); ?>">
      <div class="form-group">
      <label for="usr">Label:</label>
      <select id="select" name="label[]" class="form-control"  multiple="multiple"required="">
      <option value="infaq" <?php if (in_array('infaq', $array)) { echo 'selected'; } ?>>infaq</option>
      <option value="lazizmu" <?php if (in_array('lazizmu', $array)) { echo 'selected'; } ?> >lazizmu</option>
      <option value="muhammadiyah" <?php if (in_array('muhammadiyah', $array)) { echo 'selected'; } ?>>muhammadiyah</option>
      <option value="sedekah" <?php if (in_array('sedekah', $array)) { echo 'selected'; } ?>>sedekah</option>
      <option value="zakat" <?php if (in_array('zakat', $array)) { echo 'selected'; } ?>>zakat</option>
      <option value="rakerwil"<?php if (in_array('rakerwil', $array)) { echo 'selected'; } ?>>rakerwil</option>
      <option value="kisah_inpirasi" <?php if (in_array('kisah_inspirasi', $array)) { echo 'selected'; } ?>>kisah_inspirasi</option>
      </select>
      </div>
      <div class="form-group">
      <label for="usr">Gambar:</label>
      <input type="file" class="form-control" id="usr" name="gambar">
      <img src="<?= base_url('gambar_artikel/').$artikel->gambar ?>" width="60" heigth="60">
      </div>
      <div class="form-group">
      <label for="usr">Keyword:</label>
      <input type="text" class="form-control" id="usr" name="keyword" value="<?= $artikel->keyword ?>">
      </div>
      <input type="hidden" name="date" value="<?=date("Y-m-d")  ?>">
      </div>
      <label for="usr">Isi Konten:</label>
      <div class="box-body pad">
                    <textarea id="editor1" name="content" rows="10" cols="80" style="visibility: hidden; display: none;"> <?= $artikel->content ?>
                    </textarea>
                    <br>
                    <button type="submit" class="btn btn-primary btn-lg">Save Me</button>

              </form>
            </div>
          </div>
          <!-- /.box -->

  
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <script>$(document).ready(function() {
    $('#select').select2();
});</script>