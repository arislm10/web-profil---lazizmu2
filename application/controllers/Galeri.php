<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {


	public function index()
	{
        $data['gambar'] = $this->db->get('tb_galeri'); 
        $data['content'] = 'galeri/v_galeri';
		$this->load->view('layout/temp',$data);
    }
    public function tambah_galeri() {
        $gambar = round(microtime(true) * 1000);
        $title = $this->input->post('title');
        $desc =  $this->input->post('deskripsi');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'gambar-'.$gambar;
        $config['overwrite']			= true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }
        $data = array(
            'title' => $title,
            'deskripsi' => $desc,
            'gambar' => $image
        );
        print_r($data);
       
        $this->db->insert('tb_galeri',$data);
        $this->session->set_flashdata('success', 'Data Berhasil Masuk'); 
        redirect('galeri');
    }
    public function hapus_gambar($id){
        $this->db->delete('tb_galeri', array('id_galeri' => $id));
        $this->session->set_flashdata('success', 'foto Terhapus!'); 
        redirect('galeri');
    }
    public function edit_gambar($id){
        $data['gambar'] = $this->db->get_where('tb_galeri',array('id_galeri'=> $id))->row(); 
        $data['content'] = 'galeri/edit_galeri';
		$this->load->view('layout/temp',$data);

    }
    public function edit_proses() {
        
        $title = $this->input->post('title');
        $desc =  $this->input->post('deskripsi');
        $id= $this->input->post('id');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 4024; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $image = $this->upload->data("file_name");
        }

        $data = array(
            'title' => $title,
            'deskripsi' => $desc,
        );

        if (isset($image)) {
            $data['gambar'] = $image;  
        }

        print_r($data);
  
        if(isset($data)){
            $this->db->where('id_galeri', $id);
            $update = $this->db->update('tb_galeri', $data);
 
                 if($update == TRUE){
                     $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
                     redirect('galeri');
                 }else {
                     $this->session->set_flashdata('error', 'Data gagal diubah'); 
                     redirect('galeri');
                 }
         }else {
             $this->session->set_flashdata('error', 'Data Error'); 
                     redirect('galeri');
         }
        }
}
