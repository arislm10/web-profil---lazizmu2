<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
  @page { size: landscape; }
</style>
<div class="container">         
  <table class="table table-bordered">
    <thead>
     <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>No Hp</th>
                  <th>Pekerjaan</th>
                  <th>Penghasilan</th>
                  <th>Donasi</th>
                  <th>Tanggal Donasi</th>
                  <th>Jumlah</th>
                  <th>Metode</th>
              
                </tr>
                </thead>
                <tbody>
              <?php 
                  $no = 1;
                  foreach($donatur as $g){
              ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $g->nama_lengkap  ?></td>
                  <td><?= $g->alamat ?></td>
                  <td><?=$g->email ?></td>
                  <td><?=$g->telp ?></td>
                  <td><?= $g->pekerjaan  ?></td>
                  <td><?= $g->jumlah_penghasilan ?></td>
                  <td><?= $g->donasi ?></td>
                  <td><?= $g->tanggal_donasi ?></td>
                  <td> <?= $g->jumlah ?></td>
                  <td><?= $g->nama_metode ?></td>
                </tr>
                <?php } ?>
    </tbody>
  </table>
</div>
      <script type="text/javascript">
        
$(document).ready(function(){
   setTimeout(function(){ window.print(); }, 1000);
});

      </script>
</body>
</html>
