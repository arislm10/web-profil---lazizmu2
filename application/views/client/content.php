
  <style>
  /* card details start  */
@import url('https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto+Condensed:400,400i,700,700i');
section{
    padding: 100px 0;
}
.details-card {
	background: #ffffff;
}

.card-content {
	background: #ffffff;
	border: 4px;
	box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
}

.card-img {
	position: relative;
	overflow: hidden;
	border-radius: 0;
	z-index: 1;
}

.card-img img {
	width: 100%;
	height: 200;
	display: block;
}

.card-img span {
	position: absolute;
    top: 15%;
    left: 12%;
    background: #007bff;
    padding: 6px;
    color: #fff;
    font-size: 12px;
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    -o-border-radius: 4px;
    transform: translate(-50%,-50%);
}
.card-img span h4{
        font-size: 12px;
        margin:0;
        padding:10px 5px;
         line-height: 0;
}
.card-desc {
	padding: 1.25rem;
}

.card-desc h3 {
	color: #000000;
    font-weight: 600;
    font-size: 1.5em;
    line-height: 1.3em;
    margin-top: 0;
    margin-bottom: 5px;
    padding: 0;
}

.card-desc p {
	color: #747373;
    font-size: 14px;
	font-weight: 400;
	font-size: 1em;
	line-height: 1.5;
	margin: 0px;
	margin-bottom: 20px;
	padding: 0;
	font-family: 'Raleway', sans-serif;
}
.btn-card{
	background-color: #007bff;
	color: #fff;
	box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    padding: .84rem 2.14rem;
    font-size: .81rem;
    -webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    -o-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    margin: 0;
    border: 0;
    -webkit-border-radius: .125rem;
    border-radius: .125rem;
    cursor: pointer;
    text-transform: uppercase;
    white-space: normal;
    word-wrap: break-word;
    color: #fff;
}
.btn-card:hover {
    background: orange;
}
a.btn-card {
    text-decoration: none;
    color: #fff;
}

/* End card section */ </style>
  <?php $image=$this->db->get_where('tb_layout_gambar',array('id_letak' => 2))->row() ?>
  <section id="intro" class="clearfix" style="background-image : url('<?= base_url('uploads/').$image->gambar ?>') !important">
    <div class="container">
      <div class="intro-img">
        <img src="<?php echo base_url()  ?>source/img/intro-img.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Zakat Itu Gaya Hidupku<br><span>Bagaimana</span><br>Denganmu</h2>
        <div>
          <a href="https://anggarariocopasga.000webhostapp.com/form-donatur.php" target="_blank" class="btn-get-started scrollto">DonasiKan Sebagian Rezekimu Yuk !</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Kegiatan Kami</h3>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
             <span style="color:orange"><b>Lazizmu</b></span>  bukan hanya organisasi yang bergerak di bidang zakat dan amal namun kamu juga memliki kegiatan positif yang bermanfaat bagi masyarakat dan orang lain, Berikut kegiatan kegiatan kami
            </p>
      <?php foreach($kegiatan as $k ) {
        
        ?>
            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-arrow-circle-right"></i></div>
              <h4 class="title"><a href=""><?= $k->nama_kegiatan ?></a></h4>
              <p class="description"><?= $k->deskripsi ?></p>
            </div>
      <?php } ?>
          </div>
          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
          <?php $image=$this->db->get_where('tb_layout_gambar',array('id_letak' => 3))->row() ?>
            <img src="<?= base_url('uploads/').$image->gambar ?>" class="img-fluid" alt="" style="margin-top: 100px;">
          </div>
        </div>



      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
        <header class="section-header">
          <h3>Latest Post</h3>
        </header>
        <section class="details-card">
    <div class="container">
        <div class="row">
        <?php 
        $this->db->limit(3);
         $this->db->order_by("tanggal_posting", "desc");
        $artikel= $this->db->get('tb_artikel')->result();
        foreach($artikel as $art) {
         
        ?>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img img src="<?= base_url('gambar_artikel/').$art->gambar ?>" class="img-responsive" height="200" alt="">
                        <span><h4>heading</h3></span>
                    </div>
                    <div class="card-desc">
                        <b><p><?= $art->judul ?></p></b>
                        <p><?= word_limiter($art->content,10) ?></p>
                            <a href="<?= site_url('artikel/lihat_artikel/').$art->id_artikel ?>" class="btn btn-primary btn-rounded">Read More</a>   
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</section>

      </div>
    </section>
  