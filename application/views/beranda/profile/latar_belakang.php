<br>

<br>
<section id="portfolio" class="clearfix">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title"><?= $latar_belakang->title ?></h3>
        </header>

        <div class="row">

        </div>

        <div class="container">
        <div class="row about-extra">
          <div class="col-lg-12 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <?= $latar_belakang->content ?>
          </div>
          
        </div>
        </div>


      </div>
    </section><!-- #portfolio -->