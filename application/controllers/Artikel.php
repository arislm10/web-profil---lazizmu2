<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {


	public function index()
	{ 	
       
        $data['content'] = 'beranda/artikel/v_artikel';
        $this->load->helper('text');
        $data['kategori'] = $this->db->get('tb_kategori')->result();
		$this->db->select('*');
		$this->db->from('tb_artikel');
        $this->db->join('tb_kategori','tb_artikel.id_kategori = tb_kategori.id_kategori');
		$data['artikel'] = $this->db->get()->result();
		$this->load->view('layout/temp',$data);
    }
    public function cari_artikel() {
    $this->load->model('model_utama');
    if(isset($_GET['kata_kunci'])){
    
        $kata_kunci = htmlentities(trim($_GET['kata_kunci']));
        if(strlen($kata_kunci)<3){
            echo '<p>Kata kunci terlalu pendek.</p>';
        }else{
            $where = "";
            
            $kata_kunci_split = preg_split('/[\s]+/', $kata_kunci);
            $total_kata_kunci = count($kata_kunci_split);
            
            foreach($kata_kunci_split as $key=>$kunci){
                $where .= "judul LIKE '%$kunci%'";
                if($key != ($total_kata_kunci - 1)){
                    $where .= " OR ";
                }
            }
            
        
            $results = $this->db->query("SELECT judul, LEFT(content, 20) as deskripsi FROM tb_artikel WHERE $where");
            //menghitung jumlah hasil query di atas
            $num = $results->num_rows();
            //jika tidak ada hasil
            if($num == 0){
                //pesan jika tidak ada hasil
                $data['content'] = 'beranda/artikel/artikel';
                $data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
                $data['total']  = $this->model_utama->totalpengunjung()->row_array();
                $data['hits']             = $this->model_utama->hits()->row_array();
                $data['totalhits']        = $this->model_utama->totalhits()->row_array();
                $data['online'] = $this->model_utama->pengunjungonline()->num_rows();
                $data['artikel'] = NULL;
                $nu['num'] = $num;
                $this->load->library('pagination');
                $this->load->helper('text');
                $data['content'] = 'beranda/artikel/artikel';
                $data['kategori'] = $this->db->get('tb_kategori')->result();
                $jumlah_data = $num;
                $config['base_url'] = base_url().'index.php/beranda/artikelku';
                $config['total_rows'] = $jumlah_data;
                $config['per_page'] = 4;
                $config['num_links'] = 2; 
                $config['first_link']       = 'First';
                $config['last_link']        = 'Last';
                $config['next_link']        = 'Next';
                $config['prev_link']        = 'Prev';
                $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
                $config['full_tag_close']   = '</ul></nav></div>';
                $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
                $config['num_tag_close']    = '</span></li>';
                $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
                $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
                $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
                $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['prev_tagl_close']  = '</span>Next</li>';
                $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
                $config['first_tagl_close'] = '</span></li>';
                $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['last_tagl_close']  = '</span></li>';
                $from = $this->uri->segment(3);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();    
                $this->load->view('layout/clientTmp',$data);
                
            }else{
             
                //pesan jika ada hasil pencarian
                // echo '<p>Pencarian dari kata kunci <b>'.$kata_kunci.'</b> mendapatkan '.$num.' hasil:</p>';
                $data['content'] = 'beranda/artikel/artikel';
                $data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
                $data['total']  = $this->model_utama->totalpengunjung()->row_array();
                $data['hits']             = $this->model_utama->hits()->row_array();
                $data['totalhits']        = $this->model_utama->totalhits()->row_array();
                $data['online'] = $this->model_utama->pengunjungonline()->num_rows();
                $data['number'] = $num;
                $data['keyword'] = $_GET['kata_kunci'];
                $this->load->library('pagination');
                $this->load->helper('text');
                $data['content'] = 'beranda/artikel/artikel';
                $data['kategori'] = $this->db->get('tb_kategori')->result();
                $jumlah_data = $num;
                $config['base_url'] = base_url().'index.php/beranda/artikelku';
                $config['total_rows'] = $jumlah_data;
                $config['per_page'] = 4;
                $config['num_links'] = 2; 
                $config['first_link']       = 'First';
                $config['last_link']        = 'Last';
                $config['next_link']        = 'Next';
                $config['prev_link']        = 'Prev';
                $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
                $config['full_tag_close']   = '</ul></nav></div>';
                $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
                $config['num_tag_close']    = '</span></li>';
                $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
                $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
                $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
                $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['prev_tagl_close']  = '</span>Next</li>';
                $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
                $config['first_tagl_close'] = '</span></li>';
                $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['last_tagl_close']  = '</span></li>';
                $from = $this->uri->segment(3);
                $this->pagination->initialize($config); 
                $data['pagination'] = $this->pagination->create_links();    
                // $this->db->select('*');
                // $this->db->join('tb_kategori','tb_artikel.id_kategori = tb_kategori.id_kategori');
                $data['artikel'] = $this->db->query("SELECT * FROM tb_artikel INNER JOIN tb_kategori ON tb_artikel.id_kategori = tb_kategori.id_kategori WHERE $where")->result_array();
                $this->load->view('layout/clientTmp',$data);
            }
        }
    }
}
    public function hapus_artikel($id){
        $this->db->delete('tb_artikel', array('id_artikel' => $id));
        $this->session->set_flashdata('error', 'Artikel Terhapus!'); 
        redirect('artikel');
    }
    public function lihat_artikel($id)
	{ 	
        $this->load->model('model_utama');
		$data['pengunjung']       = $this->model_utama->pengunjung()->num_rows();
		$data['total']  = $this->model_utama->totalpengunjung()->row_array();
		$data['hits']             = $this->model_utama->hits()->row_array();
		$data['totalhits']        = $this->model_utama->totalhits()->row_array();
		$data['online'] = $this->model_utama->pengunjungonline()->num_rows();
        $data['content'] = 'beranda/artikel/lihat_artikel';
        $data['kategori'] = $this->db->get('tb_kategori')->result();
		$this->db->select('*');
		$this->db->from('tb_artikel');
        $this->db->join('tb_kategori','tb_artikel.id_kategori = tb_kategori.id_kategori');
        $this->db->where('id_artikel', $id);
		$data['artikel'] = $this->db->get()->row();
		$this->load->view('layout/clientTmp',$data);
    }
    public function edit_artikel($id)
	{ 	
			
        $data['content'] = 'beranda/artikel/edit_artikel';
        $data['kategori'] = $this->db->get('tb_kategori')->result();
		// $this->db->select('*');
        // $this->db->join('tb_kategori','tb_kategori.id_kategori = tb_artikel.id_artikel');
        // $this->db->where('tb_artikel.id_artikel',$id);
		$data['artikel'] = $this->db->get_where('tb_artikel',array('id_artikel'=> $id))->row();
		$this->load->view('layout/temp',$data); 
    }
    

    public function post_artikel()
	{ 	
		
		$data['content'] = 'beranda/artikel/post_artikel';
		$this->load->view('layout/temp',$data);
    }

    public function post_artikel_proses()
	{ 	
		$title = $this->input->post('title');
        $keyword = $this->input->post('keyword');
        $isi = $this->input->post('content');
        $date = $this->input->post('date');
        $kategori = $this->input->post('kategori');
        $label = $this->input->post('label');
        $lab = implode(" ",$label);
        $email = $label = $this->input->post('email');

        $config['upload_path']          = './gambar_artikel/';
        $config['allowed_types']        = 'svg|gif|jpg|png';
        $config['overwrite']			= true;
        $config['max_size']             = 3000; 

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
           $image= $this->upload->data("file_name");
        }

        $data = array(

            'judul' => $title,
            'content' => $isi,
            'tanggal_posting' => $date,
            'keyword'=> $keyword,
            'label' => $lab,
            'id_kategori' => $kategori,
            'gambar' => $image,
            'email' => $email
        );
        
        print_r($data);
        $this->db->insert('tb_artikel',$data);
        $this->session->set_flashdata('success', 'Berhasil Posting, Cek Beranda berita'); 
        redirect('artikel');
    }

    public function edit_artikel_proses(){
        
        { 	
            $title = $this->input->post('title');
            $keyword = $this->input->post('keyword');
            $isi = $this->input->post('content');
            $kategori = $this->input->post('kategori');
            $label = $this->input->post('label');
            $lab = implode(" ",$label);
            $email = $this->input->post('email');
            $id=  $this->input->post('id');
            $config['upload_path']          = './gambar_artikel/';
            $config['allowed_types']        = 'svg|gif|jpg|png|JPG';
            $config['overwrite']			= true;
            $config['max_size']             = 3000; 
            $this->load->library('upload', $config);
 
            if ($this->upload->do_upload('gambar')) {
               $image = $this->upload->data("file_name");
            }

            $data = array(
    
                'judul' => $title,
                'content' => $isi,
                'keyword'=> $keyword,
                'label' => $lab,
                'id_kategori' => $kategori,
                'email' => $email
            );

        if(isset($image)) {
            $data['gambar'] = $image;  
            }

        // print_r($data);
            
        if(isset($data)){
            $this->db->where('id_artikel', $id);
            $update = $this->db->update('tb_artikel', $data);
 
                 if($update == TRUE){
                     $this->session->set_flashdata('success', 'Artikel Berhasil diubah'); 
                     redirect('artikel');
                 }else {
                     $this->session->set_flashdata('error', 'Artikel gagal diubah'); 
                     redirect('artikel');
                 }
         }else {
             $this->session->set_flashdata('error', 'Data Error'); 
                     redirect('artikel');
         }
       }
    }
   
}    
    ?>