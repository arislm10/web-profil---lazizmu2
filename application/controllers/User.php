<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct(){

        parent::__construct();
        cek_login();
        cek_hakakses(array(1));
    }

    public function index()
    {
        $data['content'] = 'user/v_user';
        $data['level'] = $this->db->get('tb_admin')->result();
        $data['user'] = $this->db->query('SELECT *
        FROM tb_admin
        INNER JOIN tb_level ON tb_admin.id_level=tb_level.id_level');
        $this->load->view('layout/temp',$data);
    }

 
    public function edit_lihat($id)
    {
        $data['content'] = 'user/edit_user';
        $data['user'] = $this->db->get_where('tb_admin',array('id_admin'=>$id))->row();
        $data['l'] = $this->db->get('tb_level')->row();
        $this->load->view('layout/temp',$data);
    }   
    
    public function tambah_user()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $level = $this->input->post('level');

        $data = array(
            'email' => $email,
            'password' => $password,
            'id_level' => $level
        );
        $this->session->set_flashdata('success', 'Berhasil tambah User'); 
        $this->db->insert('tb_admin',$data);
        redirect('user');
    }

 
public function hapus_user($id){
    $this->db->delete('tb_admin', array('id_admin' => $id));
    $this->session->set_flashdata('success', 'user Terhapus!'); 
    redirect('user');
 }
 public function edit_user(){
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $level = $this->input->post('level');

        $data = array(
            'email' => $email,
            'password' => $password,
            'id_level' => $level
        );

    if(isset($data)){
        $this->db->update('tb_admin', $data, "id_admin = $id ");
        $this->session->set_flashdata('success', 'Data Berhasil diubah'); 
        redirect('user');
    }else {
        $this->session->set_flashdata('error', 'Data gagal diubah'); 
        redirect('user');
    } 
    redirect('user');
 }
}