-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 11, 2019 at 11:38 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `okrabotx_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_level` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `email`, `password`, `id_level`) VALUES
(3, 'admin@gmail.com', 'qwertyu123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `id_artikel` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `content` text,
  `keyword` varchar(100) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `tanggal_posting` date DEFAULT NULL,
  `label` varchar(160) DEFAULT NULL,
  `id_kategori` int(12) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`id_artikel`, `judul`, `content`, `keyword`, `gambar`, `tanggal_posting`, `label`, `id_kategori`, `email`) VALUES
(16, 'Kebangkitan Dunia Islam Ditandai Dengan Bermunculan Kembali Para Pengusaha Muslim', '<p>Banyak faktor yang saat ini mulai bermunculan dan dinilai dapat membangkitkan peradaban Islam. faktor-faktor tersebut seperti mulai bertambahnya jumlah pengusaha muslim, ilmuan muslim, organisasi Islam, dan jumlah umat Islam yang juga kian bertambah. Terlebih lagi dengan banyaknya pengusaha muslim, karena itu juga bisa menguatkan perekonomian umat.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Itulah benang merah dalam acara seminar nasional &ldquo;Kebangkitan Dunia Islam&rdquo;, yang diselenggarakan oleh Unit Kegiatan Islam Jama&rsquo;ah Al-Anhar (UKI-JAA) Universitas Muhammadiyah Yogyakarta (UMY). Acara ini bertempat di kampus Akademi Teknologi Kulit Yogyakarta, Sabtu (7/12).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Wakil Sekjen Majelis Intelektual dan Ulama Muda Indonesia (MIUMI), Fahmi Salim selaku narasumber pada seminar ini mengatakan bahwa salah satu kebangkitan peradaban dan umat Islam bisa ditandai dengan semakin banyaknya pengusaha muslim yang muncul. Pengusaha-pengusaha muslim itulah yang bisa menguatkan ekonomi umat Islam. &ldquo;Karena ekonomi merupakan tolak ukur kesejahteraan suatu bangsa. Dan ekonomi jugalah yang dapat membangkitkan kesejahteraan umat dari keterpurukan. Apalagi sekarang sistem ekonomi Syari&rsquo;ah sudah mulai dikenalkan di dunia internasional. Jadi dari situ kita harus tetap optimis bahwa Islam ini akan bangkit,&rdquo; paparnya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Selain itu, menurut Fahmi, adanya organisasi-organisasi Islam di dunia juga menjadi tanda dari kebangkitan Islam. Sebab organisasi Islam itu dapat berperan langsung dalam kehidupan masyarakat, karena organisasi-organisasi itu yang biasanya dekat dengan masyarakat. &ldquo;Banyak juga organisasi-organisasi Islam yang bisa melepaskan belenggu penjajah di negara lain. Misalkan seperti Serikat Islam, Muhammadiyah, dan Nahdlatul Ulama yang ikut berperan melepaskan penjajahan di Indonesia,&rdquo; tuturnya.</p>\r\n', 'islam bangkit', 'Screenshot_20170227-102123.png', '2019-02-10', 'lazizmu muhammadiyah', 2, 'arisparawali@gmail.com'),
(17, 'Mahasiswa KKN UMY Edukasi Warga Penggung Dengan Bunga Telang', '<p>Mahasiswa Kuliah Kerja Nyata (KKN) Kelompok 060 Universitas Muhammadiyah Yogyakarta (UMY) mengadakan sosialisasi budidaya bunga telang kepada masyarakat di Dusun Penggung, Desa Giripurwo, Kecamatan Girimulyo, Kabupaten Kulonprogro. Tanaman yang berfungsi sebagai obat herbal ini diedukasikan kepada masyarakat agar bisa dibudidayakan untuk mendapatkan manfaat bunga Telang yang dapat meningkatkan kesehatan, meningkatkan produktivitas perekonomian, dan menjadikannya sebagai tanaman khas Dusun Penggung.</p>\r\n\r\n<p>Sosialisasi dan edukasi yang dilakukan di rumah Kepala Dukuh Dusun Penggung sekaligus sebagai posko KKN 060 UMY diadakan pada Minggu (3/2). Selain itu juga dihadiri oleh ibu-ibu warga Penggung dan kelompok Karang Taruna.</p>\r\n\r\n<p>&ldquo;Tujuan kami memilih mensosialisasikan bungan Telang ini kepada masyarakat Dusun Penggung ini agar bisa menambah pembudidaya bunga Telang yang ada di Jogja. Karena baru sedikit masyarakat yang sudah mengetahui dan membudidayakan bunga Telang ini padahal manfaatnya sangat banyak,&rdquo; kata Muhammad Iqbal Rasyid selaku Ketua Kelompok KKN 060 UMY.</p>\r\n\r\n<p>Bunga Telang adalah tanaman merambat yang merupakan tanaman asli Asia tropis. Tanaman ini memiliki daun dan batang yang berukuran kecil. Bunganya berwarna biru gelap memiliki panjang antara 4-5 cm. Manfaat yang menakjubkan bagi kesehatan, diantaranya adalah untuk menjaga kesehatan, mencegah penuaan dini, mengobati gangguan pengelihatan, dan masih banyak lagi.</p>\r\n', 'KKN', 'maxresdefault_(1).jpg', '2019-02-10', 'lazizmu', 2, 'arisparawali@gmail.com'),
(18, 'Persiapkan Masa Depan Remaja Dari Segi Medis, Ners UMY Resmikan Posyandu Remaja di Dusun Petung', '<p>Seperti yang kita tahu bahwa kesehatan merupakan hal yang penting, bukan hanya kesehatan jasmani namun juga kesehatan rohani harus dijaga sejak dini. Terutama pada masa remaja, dimana di masa inilah mereka banyak mencoba hal baru, namun tidak sedikit remaja di Indonesia yang terdampak hal negatif secara kesehatan, seperti HIV, narkoba, pola hidup yang kurang sehat, dan lain sebagainya.</p>\r\n\r\n<p>Sebenarnya di Indonesia sejak lama sudah ada solusi mengenai hal tersebut yaitu dengan adanya posyandu. Selama ini mungkin banyak masyarakat yang hanya tahu tentang posyandu anak, namun sebenarnya ada juga posyandu remaja dan lansia di Indonesia. Di posyandu remaja inilah, remaja dapat mengontrol dan menjaga pola hidupnya secara kongkrit.</p>\r\n\r\n<p>Untuk mengatasi masalah tersebut salah satu usaha nyata yang dilakukan oleh Ners UMY 2014 dalam program perofesi yang dilaksanakan di Dusun Petung Bangunjiwo Kasihan Bantul, membantu untuk kembali mengenalkan dan menghidupkan posyandu remaja di sana.</p>\r\n\r\n<p>Posyandu remaja yang bernama Nahwa ashihhah yang artinya menuju sehat ini telah diresmikan oleh Pardja Kepala desa Bangunjiwo beserta masyarakan desa Petung juga Aceng yang mewakili Puskesmas Kasihan 1 Bantul, pada Minggu (27/1).</p>\r\n\r\n<p>&ldquo;Sebelumnya di sini belum ada posyandu remaja, baru ada posyandu lansia dan balita saja, dengan bantuan dari UMY kini dari 19 dusun di Bangunjiwo, kami menjadi dusun ke 3 yang sudah lengkap posyandunya,&rdquo; ungkap Suratman dukuh Dusun Petung.</p>\r\n\r\n<p>Suratman juga mengaku sangat berterimakasih kepada UMY, karena harapanya remaja Petung akan menjadi generasi yang baik di masa depan karena telah menjaga dan memiliki pengetahuan tentang penyakit pada remaja. Selain itu juga mencegah hal negatif yang biasanya timbul di masa remaja, seperti pengetahuan tentang NAPSA, pubertas, HB, pengetahuan tentang seks juga reproduksi dan sebagainya.</p>\r\n\r\n<p>&ldquo;Harapan saya posyandu ini dapat terus berkelanjutan karena saya lihat kader-kader posyandu Nahwa ashihhah, yakni anak-anak kami dari dusun Petung dapat menjadi generasi yang unggul di masa depan,&rdquo; ungkap Suratman.</p>\r\n\r\n<p>Ema Waliyanti, S.Kep., Ns., MPH dosen Ilmu Keperawatan Fakultas Kedokteran dan Ilmu Kesehatan UMY menambahkan, sejak tahun 2000 UMY mengabdi untuk masyarakat Bangunjiwo. Menurutnya posyandu remaja merupakan wadah bagi generasi muda untuk mempersiapkan diri menghadapi masa depan dari segi medis. Dalam acara ini selain diadakanya peresmian posyandu, juga dilakukan pemeriksaan oleh mahasiswa profesi ners UMY yang dibantu oleh petugas Puskesmas Kasihan 1. (Pras)</p>\r\n', 'UMY', '0e82691159e441eb758d54cfd8ccc6c66a934856_hq.jpg', '2019-02-10', 'muhammadiyah', 1, 'arisparawali@gmail.com'),
(19, 'KKN Menjadi Berkah', '<p>Seperti yang kita tahu bahwa kesehatan merupakan hal yang penting, bukan hanya kesehatan jasmani namun juga kesehatan rohani harus dijaga sejak dini. Terutama pada masa remaja, dimana di masa inilah mereka banyak mencoba hal baru, namun tidak sedikit remaja di Indonesia yang terdampak hal negatif secara kesehatan, seperti HIV, narkoba, pola hidup yang kurang sehat, dan lain sebagainya.</p>\r\n\r\n<p>Sebenarnya di Indonesia sejak lama sudah ada solusi mengenai hal tersebut yaitu dengan adanya posyandu. Selama ini mungkin banyak masyarakat yang hanya tahu tentang posyandu anak, namun sebenarnya ada juga posyandu remaja dan lansia di Indonesia. Di posyandu remaja inilah, remaja dapat mengontrol dan menjaga pola hidupnya secara kongkrit.</p>\r\n\r\n<p>Untuk mengatasi masalah tersebut salah satu usaha nyata yang dilakukan oleh Ners UMY 2014 dalam program perofesi yang dilaksanakan di Dusun Petung Bangunjiwo Kasihan Bantul, membantu untuk kembali mengenalkan dan menghidupkan posyandu remaja di sana.</p>\r\n\r\n<p>Posyandu remaja yang bernama Nahwa ashihhah yang artinya menuju sehat ini telah diresmikan oleh Pardja Kepala desa Bangunjiwo beserta masyarakan desa Petung juga Aceng yang mewakili Puskesmas Kasihan 1 Bantul, pada Minggu (27/1).</p>\r\n\r\n<p>&ldquo;Sebelumnya di sini belum ada posyandu remaja, baru ada posyandu lansia dan balita saja, dengan bantuan dari UMY kini dari 19 dusun di Bangunjiwo, kami menjadi dusun ke 3 yang sudah lengkap posyandunya,&rdquo; ungkap Suratman dukuh Dusun Petung.</p>\r\n\r\n<p>Suratman juga mengaku sangat berterimakasih kepada UMY, karena harapanya remaja Petung akan menjadi generasi yang baik di masa depan karena telah menjaga dan memiliki pengetahuan tentang penyakit pada remaja. Selain itu juga mencegah hal negatif yang biasanya timbul di masa remaja, seperti pengetahuan tentang NAPSA, pubertas, HB, pengetahuan tentang seks juga reproduksi dan sebagainya.</p>\r\n\r\n<p>&ldquo;Harapan saya posyandu ini dapat terus berkelanjutan karena saya lihat kader-kader posyandu Nahwa ashihhah, yakni anak-anak kami dari dusun Petung dapat menjadi generasi yang unggul di masa depan,&rdquo; ungkap Suratman.</p>\r\n\r\n<p>Ema Waliyanti, S.Kep., Ns., MPH dosen Ilmu Keperawatan Fakultas Kedokteran dan Ilmu Kesehatan UMY menambahkan, sejak tahun 2000 UMY mengabdi untuk masyarakat Bangunjiwo. Menurutnya posyandu remaja merupakan wadah bagi generasi muda untuk mempersiapkan diri menghadapi masa depan dari segi medis. Dalam acara ini selain diadakanya peresmian posyandu, juga dilakukan pemeriksaan oleh mahasiswa profesi ners UMY yang dibantu oleh petugas Puskesmas Kasihan 1. (Pras)</p>\r\n', 'KKN', '22894272_1542814229145773_7932530699391969098_n.jpg', '2019-02-10', 'lazizmu muhammadiyah', 1, 'arisparawali@gmail.com'),
(20, 'jaringan1', '<p>lkll</p>\r\n', 'history1', '11212.PNG', '2019-02-10', 'lazizmu', 2, 'arisparawali@gmail.com'),
(21, 'Bagaimana Islam Politik Bangkit di Indonesia dan Berusaha Mengendalikannya', '<p>Meskipun populasi Indonesia yang berjumlah kurang lebih 260 juta ini 87 persennya Muslim, di negara ini juga ada jutaan umat Buddha, Kristen dan Hindu serta ratusan kelompok etnis. Dan lebih dari 1 persen merupakan etnis China, seperti Ahok.</p>\r\n\r\n<p>Islam di Indonesia sering dikatakan lebih moderat daripada di Timur Tengah, sebagian karena tradisi animisme, Hindu dan Budha sebelumnya memberikannya karakter yang lebih sinkretis. Meskipun itu benar sebagai perbandingan umum, evolusi&nbsp;<a href=\"https://www.matamatapolitik.com/tag/islam-politik\">Islam politik</a>&nbsp;di Indonesia telah jauh lebih diributkan dan sangat berbeda dari apa yang ditunjukkan oleh narasi ini.</p>\r\n\r\n<p>Faktanya, selama beberapa dekade sejak kemerdekaan resmi Indonesia pada tahun 1949, para pemimpin telah mengadopsi berbagai pendekatan berbeda untuk menciptakan keseimbangan antara mengakui peran Islam sambil berusaha mengendalikannya sebagai kekuatan dalam politik.</p>\r\n\r\n<p>Di bawah Soeharto, diktator yang memerintah Indonesia dari 1967 hingga 1998, Islam politik ditekan untuk mendukung ideologi Pancasila, filosofi pendirian negara Indonesia yang percaya pada &ldquo;keabsahan&rdquo; satu Tuhan tetapi tidak menempatkan Islam sebagai satu-satunya agama resmi di Indonesia. Tetapi sejak periode Reformasi yang diikuti kejatuhan Soeharto pada tahun 1998 dan menyebabkan terbukanya masyarakat dan politik, Islam politik telah mendapatkan lebih banyak ruang untuk beroperasi.</p>\r\n\r\n<p>Hal itu memiliki untung ruginya. Di satu sisi, partai-partai Islamis moderat&mdash;termasuk yang terbesar, Nadhlatul Ulama (NU) dan Muhammadiyah, yang memiliki puluhan juta pengikut&mdash;memiliki ruang yang lebih besar untuk menyediakan saluran bagi ekspresi politik yang damai dan berfungsi sebagai benteng yang efektif terhadap lebih banyak kekuatan radikal dan kekerasan. Hal ini sering disebut sebagai salah satu alasan mengapa begitu sedikit orang Indonesia yang bergabung dengan apa yang disebut Negara Islam (IS) daripada negara-negara Muslim lainnya, dan mengapa ideologi fundamentalis kelompok itu telah begitu terkutuk di negara ini.</p>\r\n\r\n<p>Namun di sisi lain, ruang politik pasca-Soeharto telah dieksploitasi oleh kelompok-kelompok garis keras seperti Front Pembela Islam, atau FPI, yang telah menggerogoti hak-hak minoritas agama&mdash;termasuk Adhmadiyah, Kristen dan Muslim Syiah&mdash;dan mempertanyakan kredensial Islam para pemimpin Indonesia. Selama kepemimpinannya dari tahun 2004 hingga 2014, mantan Presiden Susilo Bambang Yudhoyono enggan untuk menindak keras para militan Islam karena takut mengurangi popularitasnya di kalangan kelompok Muslim konservatif.</p>\r\n', 'Politik', '11212.PNG', '2019-02-10', 'lazizmu muhammadiyah', 1, 'arisparawali@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tb_contact`
--

CREATE TABLE `tb_contact` (
  `id_contact` int(12) NOT NULL,
  `nama_contact` varchar(90) DEFAULT NULL,
  `fb` varchar(90) DEFAULT NULL,
  `twitter` varchar(90) DEFAULT NULL,
  `whatsapp` varchar(90) DEFAULT NULL,
  `ig` varchar(90) DEFAULT NULL,
  `no_telp` varchar(90) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contact`
--

INSERT INTO `tb_contact` (`id_contact`, `nama_contact`, `fb`, `twitter`, `whatsapp`, `ig`, `no_telp`, `deskripsi`) VALUES
(1, 'lazizmu umbulharjo1', 'https://www.facebook.com/profile.php?id=100011430304303', 'https://twitter.com/lazismuuh?lang=id', '08995051540', 'https://www.instagram.com/lazismuumbulharjo/', '(0274) 380041', 'atau Lembaga Zakat Infaq dan Shadaqah Muhammadiyah adalah lembaga zakat tingkat nasional yang berkhidmat dalam pemberdayaan masyarakat melalui pendayagunaan secara produktif dana zakat, infaq, sadaqah, wakaf dan dana kedermawanan lainnya baik dari perseorangan, lembaga, perusahaan dan instansi lainnya.');

-- --------------------------------------------------------

--
-- Table structure for table `tb_donasi`
--

CREATE TABLE `tb_donasi` (
  `id_donasi` int(12) DEFAULT NULL,
  `title` varchar(40) DEFAULT NULL,
  `content` text,
  `tanggal_update` date DEFAULT NULL,
  `keyword` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_donasi`
--

INSERT INTO `tb_donasi` (`id_donasi`, `title`, `content`, `tanggal_update`, `keyword`) VALUES
(1, 'Donasi Yuk!', '<p>Untuk melakukan Donasi melalui Lazismu D.I. Yogyakarta dapat melalui rekening berikut ini:</p>\r\n\r\n<p><img src=\"http://www.lazismudiy.or.id/wp-content/uploads/2016/12/bdw.jpg\" style=\"height:32px; width:39px\" />&nbsp; No. Rekening: 24.2738 a.n. LAZISMU DIY, Untuk Infaq dan Shadaqah</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.lazismudiy.or.id/wp-content/uploads/2016/12/bsb.png\" style=\"height:28px; width:75px\" />&nbsp;No. Rekening: 7709002300 a.n. LAZISMU D.I.YOGYAKARTA,&nbsp;Untuk Infaq dan Shadaqah</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.lazismudiy.or.id/wp-content/uploads/2016/12/bni-syariah.png\" style=\"height:45px; width:45px\" />&nbsp;No. Rekening: 0452599825 a.n. LAZISMU DIY, Untuk Dana Kemanusian dan Kebencanaan</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.lazismudiy.or.id/wp-content/uploads/2016/12/bsm.png\" style=\"height:29px; width:59px\" />No. Rekening: 2256565229 a.n. LAZISMU DIY, Untuk Zakat</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2019-02-07', 'donasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_galeri`
--

CREATE TABLE `tb_galeri` (
  `id_galeri` int(12) NOT NULL,
  `deskripsi` varchar(50) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `title` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_galeri`
--

INSERT INTO `tb_galeri` (`id_galeri`, `deskripsi`, `gambar`, `title`) VALUES
(20, 'Bakti Sosial di jalan Merpati sabtu 3 februri 2019', 'flayer-3.jpg', 'Bakti Sosial1'),
(21, 'wewewe', 'gambar-1549534825002.PNG', 'SUSUNAN PENGELOLA PERWAKILAN LEMBAGA AMI'),
(22, 'ssds', 'logo-3-e1465750111120.png', 'test1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jaringan`
--

CREATE TABLE `tb_jaringan` (
  `id_jaringan` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `tgl_update` date DEFAULT NULL,
  `keyword` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jaringan`
--

INSERT INTO `tb_jaringan` (`id_jaringan`, `title`, `content`, `tgl_update`, `keyword`) VALUES
(1, 'Jaringan', '<p>Untuk terus menghidmatkan diri dalam pengabdian dan pembelaan kepada masyarakat lemah melalui program-program yang sudah disusun, maka Lazismu D.I. Yogyakarta melakuakn kerjasama dengan seluruh pihak-pihak yang terkait dalam upaya untuk tercapainya program-program dimaksud. Pihak-pihak yang dinamakan sebagai Jaringan Lazismu D.I. Yogyakarta di antaranya:</p>\r\n\r\n<ol>\r\n	<li><a href=\"http://www.saudagarmuhammadiyah.com/\">Jaringan Saudagar Muhammadiyah (JSM) D.I. Yogyakarta</a></li>\r\n	<li>RSU PKU Muhammadiyah Kota Yogyakarta</li>\r\n	<li>RSU PKU Muhammadiyah Bantul</li>\r\n	<li>RSKIA PKU Muhammadiyah Kotagede</li>\r\n	<li>PT. BUMY HARAPAN UMAT (BUHARUM)</li>\r\n	<li>PT. Surya Citra Madani (SCM)</li>\r\n	<li>Depot Muhammadiyah</li>\r\n	<li>Amal Usaha Muhammadiyah Bidang Pendidikan</li>\r\n	<li>Amal Usaha Muhammadiyah Bidang Sosial</li>\r\n	<li>dan lainnya</li>\r\n</ol>\r\n', '2019-02-11', 'Jaringan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(12) NOT NULL,
  `nama_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Berita'),
(2, 'Kisah Inspirasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_layanan`
--

CREATE TABLE `tb_layanan` (
  `id_layanan` int(11) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `content` text,
  `tanggal_update` date DEFAULT NULL,
  `keyword` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_layanan`
--

INSERT INTO `tb_layanan` (`id_layanan`, `title`, `content`, `tanggal_update`, `keyword`) VALUES
(1, 'Panduan Donasi', '<p><strong>Donasi Melalui Website :</strong></p>\r\n\r\n<ol>\r\n	<li>Akses Halaman Website</li>\r\n	<li>Pilih tombol : layanan</li>\r\n	<li>Lengkapi formulir pendaftran donasi &nbsp;:<a href=\"https://anggarariocopasga.000webhostapp.com/form-donatur.php\">Pendaftaran Donatur</a></li>\r\n	<li>Konfirmasi melalui layanan :&nbsp;<a href=\"https://anggarariocopasga.000webhostapp.com/konfirmasi.php\">Konfirmasi Donasi</a></li>\r\n</ol>\r\n\r\n<p><strong>Jemput Donasi :</strong></p>\r\n\r\n<ol>\r\n	<li>Klik Menu Layanan Jemput Donasi</li>\r\n	<li>Lengkapi Data Diri dan Lokasi Penjemputan</li>\r\n	<li>Lokasi Masuk Jangkauan Tim</li>\r\n	<li>Tim Menuju Lokasi</li>\r\n	<li>Donasi diterima</li>\r\n	<li>Donatur mendapatkan bukti penerimaan</li>\r\n</ol>\r\n', '2019-02-10', 'donasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_layout_gambar`
--

CREATE TABLE `tb_layout_gambar` (
  `id_letak` int(12) NOT NULL,
  `nama_letak` varchar(100) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_layout_gambar`
--

INSERT INTO `tb_layout_gambar` (`id_letak`, `nama_letak`, `gambar`) VALUES
(1, 'header', 'logo-3-e1465750111120.png'),
(2, 'backgroud_awal', 'intro-bg.png'),
(3, 'backgroud_awal', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_layout_kegiatan`
--

CREATE TABLE `tb_layout_kegiatan` (
  `id_layout` int(11) NOT NULL,
  `nama_kegiatan` varchar(120) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_layout_kegiatan`
--

INSERT INTO `tb_layout_kegiatan` (`id_layout`, `nama_kegiatan`, `deskripsi`) VALUES
(1, 'pendidikan1', 'Ramadhan telah berlalu di bumi Kota Miyak, sebutan untuk Kota Sorong. Kota di Timur Indonesia ini merupakan pintu gerbang'),
(2, 'Dakwah1', 'Ramadhan telah berlalu di bumi Kota Miyak, sebutan untuk Kota Sorong. Kota di Timur Indonesia ini merupakan pintu gerban'),
(3, 'school kit', 'Ramadhan telah berlalu di bumi Kota Miyak, sebutan untuk Kota Sorong. Kota di Timur Indonesia ini merupakan pintu gerban'),
(4, 'kado ramadhan', 'Ramadhan telah berlalu di bumi Kota Miyak, sebutan untuk Kota Sorong. Kota di Timur Indonesia ini merupakan pintu gerban');

-- --------------------------------------------------------

--
-- Table structure for table `tb_level`
--

CREATE TABLE `tb_level` (
  `id_level` int(12) NOT NULL,
  `nama_level` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `id_profile` int(12) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` text,
  `tanggal_update` date DEFAULT NULL,
  `keyword` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`id_profile`, `title`, `content`, `tanggal_update`, `keyword`) VALUES
(1, 'Visi dan Misi Kami', '<h2>Visi 1</h2>\r\n\r\n<p>Menjadi Lembaga Amil Zakat Terpercaya</p>\r\n\r\n<h2>Misi</h2>\r\n\r\n<p>1. Optimalisasi pengelolaan ZIS yang amanah, profesional dan transparan;</p>\r\n\r\n<p>2. Optimalisasi pendayagunaan ZIS yang kreatif, inovatif dan produktif;</p>\r\n\r\n<p>3. Optimalisasi pelayanan donator</p>\r\n', '2019-02-10', 'visimisi'),
(2, 'Latar Belakang', '<p>LAZISMU adalah lembaga zakat tingkat nasional yang berkhidmat dalam pemberdayaan masyarakat melalui pendayagunaan secara produktif dana zakat, infaq, wakaf dan dana kedermawanan lainnya baik dari perseorangan, lembaga, perusahaan dan instansi lainnya.</p>\r\n\r\n<p>Didirikan oleh PP. Muhammadiyah pada tahun 2002, selanjutnya dikukuhkan oleh Menteri Agama Republik Indonesia sebagai Lembaga Amil Zakat Nasional melalui SK No. 457/21 November 2002. Dengan telah berlakunya Undang-undang Zakat nomor 23 tahun 2011, Peraturan Pemerintah nomor 14 tahun 2014, dan Keputusan Mentri Agama Republik Indonesia nomor 333 tahun 2015. LAZISMU sebagai lembaga amil zakat nasional telah dikukuhkan kembali melalui SK Mentri Agama Republik Indonesia nomor 730 tahun 2016.</p>\r\n\r\n<p>Latar belakang berdirinya LAZISMU terdiri atas dua faktor. Pertama, fakta Indonesia yang berselimut dengan kemiskinan yang masih meluas, kebodohan dan indeks pembangunan manusia yang sangat rendah. Semuanya berakibat dan sekaligus disebabkan tatanan keadilan sosial yang lemah.</p>\r\n\r\n<p>Kedua, zakat diyakini mampu bersumbangsih dalam mendorong keadilan sosial, pembangunan manusia dan mampu mengentaskan kemiskinan. Sebagai negara berpenduduk muslim terbesar di dunia, Indonesia memiliki potensi zakat, infaq dan wakaf yang terbilang cukup tinggi. Namun, potensi yang ada belum dapat dikelola dan didayagunakan secara maksimal sehingga tidak memberi dampak yang signifikan bagi penyelesaian persoalan yang ada.</p>\r\n\r\n<p>Berdirinya LAZISMU dimaksudkan sebagai institusi pengelola zakat dengan manajemen modern yang dapat menghantarkan zakat menjadi bagian dari penyelesai masalah (problem solver) sosial masyarakat yang terus berkembang.</p>\r\n\r\n<p>Dengan budaya kerja amanah, professional dan transparan, LAZISMU berusaha mengembangkan diri menjadi Lembaga Zakat terpercaya. Dan seiring waktu, kepercayaan publik semakin menguat.</p>\r\n\r\n<p>Dengan spirit kreatifitas dan inovasi, LAZISMU senantiasa menproduksi program-program pendayagunaan yang mampu menjawab tantangan perubahan dan problem sosial masyarakat yang berkembang.</p>\r\n\r\n<p>Saat ini, LAZISMU telah tersebar hampir di seluruh Indonesia yang menjadikan program-program pendayagunaan mampu menjangkau seluruh wilayah secara cepat, fokus dan tepat sasaran.</p>\r\n', '2019-02-05', 'history'),
(3, 'DAFTAR PENGURUS KANTOR LAYANAN LAZIZMU UMBULHARJO', '<p><strong>Pembimbing/Nasehat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong>: Anggota Pimpinan Cabang Muhammadiyah Umbulharjo</p>\r\n\r\n<p><strong>Pengawas Syariah</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: H.Poniman, S.Ag</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Safroni Syaefudin</p>\r\n\r\n<p><strong>Audit Syariat dan Keuangan</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: H. Rakhmat Sutomo, SE</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Muhammad Soleh, SE</p>\r\n\r\n<p><strong>Badan Pengurus</strong></p>\r\n\r\n<hr />\r\n<p><strong>Ketua</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Eman Suherman S.Sos.i</p>\r\n\r\n<p><strong>Sekertatis</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Muhammad Ridwan Muthohar , S.Pd</p>\r\n\r\n<p><strong>Bendahara</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Ir.H.Murbani</p>\r\n\r\n<p><strong>Anggota&nbsp;&nbsp; </strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : H.Suradiyono</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Badan Eksekutif</strong></p>\r\n\r\n<hr />\r\n<p><strong>Kepala Kantor</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;: Eman Suherman S.Sos.i</p>\r\n\r\n<p><strong>Keuangan</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Muhammad Ridwan Muthohar , S.Pd</p>\r\n\r\n<p><strong>Administrasi</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;: Ir.H.Murbani</p>\r\n\r\n<p><strong>Penghimpun dan Pentasyarufan&nbsp;&nbsp; </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rifai Abdul Qohat</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Satria Madjid Jatmiko</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Deky Febriasnyah</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2019-02-11', 'struktur organisasi'),
(4, 'Kebijakan Strategis', '<p><strong>Misi Pendayagunaan</strong>&nbsp;:</p>\r\n\r\n<p>Terciptanya kehidupan sosial ekonomi umat yang berkualitas sebagai benteng atas problem kemiskinan, keterbelakangan, dan kebodohan pada masyarakat melalui berbagai program yang dikembangkan Muhammadiyah.</p>\r\n\r\n<p><strong>Kebijakan Strategis Pendayagunaan:</strong></p>\r\n\r\n<p>1. Prioritas penerima manfaat adalah kelompok fakir, miskin dan fisabilillah.</p>\r\n\r\n<p>2. Pendistribusian ZIS dilakukan secara terprogram (terencana dan terukur) sesuai core gerakan Muhammadiyah, yakni: pendidikan, ekonomi, dan sosial-dakwah.</p>\r\n\r\n<p>3. Melakukan sinergi dengan majelis, lembaga, ortom dan amal-usaha Muhammdiyah dalam merealisasikan program.</p>\r\n\r\n<p>4. Melakukan sinergi dengan institusi dan komunitas diluar Muhammadiyah untuk memperluas domain dakwah sekaligus meningkatkan awareness public kepada persyarikatan.</p>\r\n\r\n<p>5. Meminimalisir bantuan karitas kecuali bersifat darurat seperti di kawasan timur Indonesia, daerah yang terpapar bencana dan upaya-upaya penyelamatan.</p>\r\n\r\n<p>6. Intermediasi bagi setiap usaha yang menciptakan kondisi dan faktor-faktor pendukung bagi terwujudnya masyarakat Islam yang sebenar-benarnya. [Visi Muhammadiyah 2025</p>\r\n\r\n<p>7. Memobilisasi pelembagaan gerakan ZIS di seluruh struktur Muhammadiyah dan amal usaha.</p>\r\n\r\n<p><strong>Sinergi Pendayagunaan</strong></p>\r\n\r\n<p>Berpijak pada posisi LAZISMU sebagai lembaga intermediate, maka dalam penyaluran dan pendayagunaan dana ziswaf bersinergi dengan berbagai lembaga baik di internal Muhammadiyah maupun lembaga diluar Muhammadiyah.</p>\r\n\r\n<p>Seperti program pendayagunaan bidang pertanian, lazismu bersinergi dengan MPM ( Majelis Pemberdayaan Masyarakat) PP Muhammadiyah, program kemanusiaan bersinergi dengan LPB PP Muhammadiyah, masalah sosial bersinergi dengan MPS Muhammadiyah, bidang ekonomi dengan MEK Muhammadiyah dan untuk pemberdayaan kaum perempuan lazismu bersinergi dengan PP &lsquo;Aisyiyah.</p>\r\n\r\n<p>Sedang sinergi dengan lembaga di luar Muhammadiyah, LAZISMU telah menggandeng berbagai lembaga dan komunitas dalam menyalurkan dan mendayagunakan dana ziswaf seperti lembaga IWAPI, komunitas WIRAMUDA, berbagai komunitas hobby dan profesi dan sebagainya.</p>\r\n\r\n<p>Tujuan dari sinergi adalah agar pendayagunaan memberi manfaat yang maksimal kepada masyarakat karena dikelola oleh lembaga pengelola yang expert serta menjangkau lokasi sasaran program yang lebih luas.</p>\r\n', '2019-02-05', 'kebijakan lazizmu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_program`
--

CREATE TABLE `tb_program` (
  `id_program` int(12) NOT NULL,
  `nama_program` varchar(100) DEFAULT NULL,
  `gambar` varchar(100) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_program`
--

INSERT INTO `tb_program` (`id_program`, `nama_program`, `gambar`, `deskripsi`) VALUES
(1, 'program tasyaruf', 'program-pentasyarufan-768x1024.png', ''),
(2, 'program penghimpunan', 'program-penghimpunan-1024x766.png', ''),
(3, 'Tiket Jariyah pelunasan hutang gedung dakwah Lazis', '', '<p>Assalamualaikum, ibu bapak yang dirahmati Allah, &nbsp;yuk jual beli sama Allah, &nbsp;melalui program tiket jariyah, &nbsp;cukup dengan minimal partisipasi 10.000 saja kita sudah bisa dapat amal jariyah.InsyaAllah donasi dermawan sekalian bisa untuk membantu kami melunasi tanggungan rehab gedung dakwah perjuangan kami, caranya mudah kok, &nbsp;bisa melalui transfer via rekening, atau bahkan bisa pakai go pay... di campaign kitabisa. Belum bisa bantu??? Tenang... Tak masalah, &nbsp;kalian bisa bantu kami broadcastkan pesan ini..&nbsp;<br />\r\nUntuk lebih lanjut bisa ngobrol dengan kang AZIS (amil zakat, &nbsp;infaq, &nbsp;sedekah).<br />\r\nTiket Jariyah pelunasan hutang rehab gedung dakwah Lazismu</p>\r\n\r\n<p>Berikut link untuk mengakses halaman galang dana kami: <a href=\"https://kitabisa.com/tiketjariyah\">https://kitabisa.com/tiketjariyah</a></p>\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`id_artikel`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `tb_galeri`
--
ALTER TABLE `tb_galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `tb_jaringan`
--
ALTER TABLE `tb_jaringan`
  ADD PRIMARY KEY (`id_jaringan`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_layanan`
--
ALTER TABLE `tb_layanan`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `tb_layout_gambar`
--
ALTER TABLE `tb_layout_gambar`
  ADD PRIMARY KEY (`id_letak`);

--
-- Indexes for table `tb_layout_kegiatan`
--
ALTER TABLE `tb_layout_kegiatan`
  ADD PRIMARY KEY (`id_layout`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `tb_program`
--
ALTER TABLE `tb_program`
  ADD PRIMARY KEY (`id_program`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_contact`
--
ALTER TABLE `tb_contact`
  MODIFY `id_contact` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_galeri`
--
ALTER TABLE `tb_galeri`
  MODIFY `id_galeri` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_jaringan`
--
ALTER TABLE `tb_jaringan`
  MODIFY `id_jaringan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_layanan`
--
ALTER TABLE `tb_layanan`
  MODIFY `id_layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_layout_gambar`
--
ALTER TABLE `tb_layout_gambar`
  MODIFY `id_letak` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_layout_kegiatan`
--
ALTER TABLE `tb_layout_kegiatan`
  MODIFY `id_layout` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id_level` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_profile`
--
ALTER TABLE `tb_profile`
  MODIFY `id_profile` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_program`
--
ALTER TABLE `tb_program`
  MODIFY `id_program` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
