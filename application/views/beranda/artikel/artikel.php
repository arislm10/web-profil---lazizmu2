<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<style>

@import url('https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600');
body{
  background-color:#f1f1f1; 
  font-family: 'Nunito Sans', sans-serif;
  width: 100%;
  height: 100%;
}
a {
   color:#333;
  -webkit-transition: all .3s;
  -moz-transition: all .3s;
  -o-transition: all .3s;
  transition: all .3s;
}
a:hover,
a:focus {
    color:#333;
    text-decoration: none;
  
}
h1,
h2,
h3,
h4,
h5 {
  letter-spacing: 0px;
  margin: 0px;
}
.img-responsive {
  width: 100%;
}
/* ------ main-page ------ */
.main-page{
    max-width:1000px;
    margin:0 auto;
}
.container{
 
}
/* ------ top-nav ------ */
.top-nav{
    padding-top:10px;
    padding-bottom:10px;
}

/* ------ main-banner ------ */
.main-banner{
    margin-bottom:20px;
}

/* ------ page ------ */
.page{
    background-color:#fff;
    padding:20px 15px;
    margin-bottom:20px;
}
.page h3{
    font-size:22px;
    margin-bottom:12px;
}
.page p{
    font-size:17px;
    font-weight:500;
}
.page .details ul{
    list-style: none;
    padding-left: 0px;
    margin-bottom:18px;
}
.page .details ul li{
   float:left;
   padding-right:15px;
   font-size: 13px;
   font-weight: 600;
}
.page .details ul li:last-child {
  float: inherit;
}
.page h4{
    font-size:13px;
    font-weight:600;
}
/* ------ wedgets ------ */
.widgets{
    background-color:#fff;
    padding:20px 15px;
    margin-bottom:20px;
}
.widgets h3{
    padding-bottom:10px;
    font-size:20px;
    margin-bottom:15px;
    border-bottom:1px solid #eaeaea;
}
.widgets ul{
    list-style: none;
    padding-left: 0px;
}
.widgets ul li{
    font-weight: 600;
    font-size: 13px;
    text-transform: uppercase;
    margin-bottom: 10px;
}
.widgets h4{
    font-size: 16px;
    font-weight: 600;
    line-height: 22px;
    letter-spacing: 0.3px;
    padding-bottom: 10px;
    border-bottom: 1px solid #e6e6e6;
}
.main-page{
    width:100%;
}
</style>
<br>

<br>

<br>
<div class="main-page">
    
   <!-- top-nav -->
   <div class="top-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6"></div>
            </div>
        </div>   
   </div>
   
   <!-- main-banner -->
    <div class="main-banner">
        <div class="container">
            <img src="https://www.sga.uni-hamburg.de/7850180/chuttersnap-1200x300-f576e44a846d72766dc57e1a343ad4c9a1281798.jpg" class="img-responsive">
        </div>
    </div>
    <div class="post-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    
                    <ol class="breadcrumb">
                      <li><a href="#"><?php if(isset($number)){
                            echo "pencarian dengan Kata kunci <b>".$keyword."</b> menemukan <b>".$number."</b> hasil";
                      }else {
                          echo "POST ARTIKEL";
                      } ?></a></li>
    
                    </ol>
                    
                    <?php 
                    if(isset($artikel)){
                    foreach($artikel as $ar) {
                        ?>
                    <div class="page">
                        <h3><?= $ar['judul'] ?></h3>
                        <div class="details">
                            <ul>
                                <li><?= $ar['email'] ?></li>
                                <li><?= $ar['tanggal_posting'] ?></li>
                                <li><?= $ar['nama_kategori'] ?></li>
                            </ul>
                        </div>
                        <div class="content">
                             <p><?=  word_limiter($ar['content'],50) ?></p>
                        </div>
                        <h4><a class="btn btn-primary btn-rounded" href="<?= site_url('artikel/lihat_artikel/').$ar['id_artikel'] ?>">Read More</a></h4>
                    </div>
                    <?php } }else{
                        echo "no result";
                    } ?>

                    <?php echo $pagination;  ?>
                </div>
                   
                <div class="col-md-4">
                    
                    <div class="widgets">
                        <h3>Search</h3>
                        <form action="<?= site_url('artikel/cari_artikel') ?>" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="kata_kunci" placeholder="Search term...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default" type="button">search</button>
                            </span>
                        </div>
                        </form>
                    </div>
                    <div class="widgets">
                        <h3>Statistik</h3>
                         <?php
                        echo "<li>User Online : $online</li>
                        <li>Today Visitor : $pengunjung</li>
                        <li>Hits hari ini : $hits[total]</li>
                        <li>Total pengunjung : $total[total]</li>";
                        ?>
                    </div>
                    <div class="widgets">
                        <h3>Categori</h3>
                        <ul>
                        <?php foreach($kategori as $val){
                            ?>
                            <li><a href="<?= site_url('beranda/artikel_kategori/').$val->id_kategori ?>"><?= $val->nama_kategori ?></a></li>
                        <?php } ?>
                        </ul>
                    </div>
                     
                    <div class="widgets" id="datepicker">
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $('#datepicker').datepicker({});
</script>