<section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">UBAH KONTAK</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?= site_url('layout/ubahKontakProses') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Kontak</label>
                  <div class="col-sm-10">
                    <input type="text" name="kontak" class="form-control" value="<?= $kontak->nama_contact ?>" placeholder="kegiatan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">url facebook</label>
                  <div class="col-sm-10">
                    <input type="text" name="fb" class="form-control" value="<?= $kontak->fb ?>" placeholder="kegiatan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">url twitter</label>
                  <div class="col-sm-10">
                    <input type="text" name="tw" class="form-control" value="<?= $kontak->twitter ?>" placeholder="kegiatan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No Whatsapp</label>
                  <div class="col-sm-10">
                    <input type="text" name="wa" class="form-control" value="<?= $kontak->whatsapp ?>" placeholder="kegiatan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Intagram</label>
                  <div class="col-sm-10">
                    <input type="text" name="ig" class="form-control" value="<?= $kontak->ig ?>" placeholder="kegiatan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">no telp</label>
                  <div class="col-sm-10">
                  <input type="text" name="telp" class="form-control" rows="3" value="<?= $kontak->no_telp ?>" placeholder="Enter ..."/>
                  </div>
                </div>
                <input type="hidden" name="id" value="<?= $kontak->id_contact ?>">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                  <textarea name="desc" class="form-control" rows="3" placeholder="Enter ..."><?= $kontak->deskripsi ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>