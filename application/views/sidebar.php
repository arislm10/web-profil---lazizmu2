
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
       
          <!-- Notifications: style can be found in dropdown.less -->
   
          <!-- Tasks: style can be found in dropdown.less -->
  
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= $this->session->userdata('email') ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?= site_url('admin/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $this->session->userdata('email') ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active"><a href="<?= site_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li> 
        <li><a href="<?= site_url('artikel') ?>"><i class="fa fa-pencil"></i> <span>Post Berita</span></a></li>
        <li><a href="<?= site_url('admin/lihat_komentar') ?>"><i class="fa fa-commenting"></i> <span>Komentar</span></a></li>
        <li><a href="<?= site_url('jaringan') ?>"><i class="fa  fa-bookmark-o"></i> <span>Jaringan</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Layanan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">   
   </li>
        <li><a href="<?= site_url('layanan') ?>"><i class="fa fa-book"></i> <span>Panduan Donasi</span></a></li>
        <li><a href="<?= site_url('donatur/view_donatur') ?>"><i class="fa fa-book"></i> <span>Donatur</span></a></li>
        <li><a href="<?= site_url('bantuan/view_bantuan') ?>"><i class="fa fa-book"></i> <span>Bantuan</span></a></li>
        <li><a href="<?= site_url('admin/konfirmasi') ?>"><i class="fa fa-book"></i> <span>Konfirmasi</span></a></li>
          </ul>
        </li>
        <li><a  href="<?= site_url('admin/statistik') ?>"><i class="fa fa-user"></i>Statistik</a></li>
        <li><a href="<?= site_url('donasi') ?>"><i class="fa fa-television"></i> <span>Donasi</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa  fa-share-alt-square"></i> <span>Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            </li>
        <li><a href="<?= site_url('profile') ?>"><i class="fa fa-circle-o"></i> <span>Latar Belakang</span></a></li>
            <li><a href="<?= site_url('profile/visi_misi') ?>"><i class="fa fa-circle-o"></i> Visi Misi</a></li>
            <li><a href="<?= site_url('profile/kebijakan_strategis') ?>"><i class="fa fa-circle-o"></i> <span>Kebijakan Strategis</span></a></li>
            <li><a href="<?= site_url('profile/struktur_organisasi') ?>"><i class="fa fa-circle-o"></i> <span>Struktur Organisasi</span></a></li>
          </ul>
        </li>
        <li><a href="<?= site_url('Galeri') ?>"><i class="fa  fa-file-image-o"></i> <span>Galeri</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa  fa-balance-scale"></i> <span>Program</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            </li>
        <li><a href="<?= site_url('program/program_tasyaruf') ?>"><i class="fa fa-circle-o"></i> <span>Program Tasyaruf</span></a></li>
            <li><a href="<?= site_url('program') ?>"><i class="fa fa-circle-o"></i> Program Peghimpun</a></li>
             <li><a href="<?= site_url('program/program_tiketJariyah') ?>"><i class="fa fa-circle-o"></i> Program Tiket Jariyah</a></li>

          </ul>
        </li>

        <?php if($this->session->userdata('id_level') == 1){
        ?>
        <li><a href="<?= site_url('layout') ?>"><i class="fa fa-book"></i> <span>Atur Gambar Layout</span></a></li>
        
       
        <li><a  href="<?= site_url('user') ?>"><i class="fa fa-user"></i>User</a></li>
        <?php } ?>
        <li><a target="_blank" href="<?= site_url('beranda') ?>"><i class="fa fa-bell"></i> Lihat Front-End</a></li>
       
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->