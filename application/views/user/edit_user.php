<section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">UBAH USER</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?= site_url('user/edit_user') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Emeil</label>
                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" value="<?= $user->email ?>" placeholder="kegiatan">
                  </div>
                </div><input type="hidden" name="id" value="<?= $this->uri->segment(3) ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" value="<?= $user->password ?>" placeholder="kegiatan">
                  </div>
                </div>
                <div class="form-group">
                <label for="" class="col-sm-2 control-label">Level</label>
                <div class="col-sm-10">
                  <select name="level" class="form-control">
                 

           <option <?php if('1' == $user->id_level) { echo "selected"; } ?> value='1'>Administrator</option>
            <option <?php if('2' == $user->id_level) { echo "selected"; } ?> value='2'>Operator</option>
                         

                  </select>
                </div>
                </div>

                    </div>
                    </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
         
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      </div>
      <!-- /.row -->
    </section>